﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ReyvFirmaAPP.Droid.Renderers;
using ReyvFirmaAPP.Views.PDFViews;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(PDFWebView), typeof(PDFWebViewRenderer))]
namespace ReyvFirmaAPP.Droid.Renderers
{
    public class PDFWebViewRenderer : WebViewRenderer
    {

        public PDFWebViewRenderer (Context context) : base(context) { }

        //El cometido del renderer es cargar la URL cuando esta se modifica
        protected override void OnElementChanged(ElementChangedEventArgs<WebView> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                PDFWebView customWebView = Element as PDFWebView;
                Control.LoadUrl(customWebView.Uri);
            }
        }
    }
}