﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using ReyvFirmaAPP.Services;
using ReyvFirmaAPP.Droid.Services;

[assembly: Dependency(typeof(Notifications))]
namespace ReyvFirmaAPP.Droid.Services
{
    public class Notifications : INotifications
    {
        public void SendNotification (string titulo, string mensaje)
        {
            //Creando el objeto de notificaciones
            Notification.Builder builder = new Notification.Builder(Android.App.Application.Context);
            builder.SetContentTitle(titulo);
            builder.SetContentText(mensaje);
            builder.SetSmallIcon(Resource.Drawable.icon);
            //builder.SetAutoCancel(true);

            //Construyendo la notificación
            Notification notification = builder.Build();

            //Obteniendo el manejador de notificaciones
            NotificationManager notificationManager =
                Android.App.Application.Context.GetSystemService(Context.NotificationService)
                as NotificationManager;

            //Creando la notificación
            notificationManager.Notify(21649, notification);
        }

        public void ShowSmallText (string mensaje)
        {
            Handler mainHandler = new Handler(Looper.MainLooper);
            Java.Lang.Runnable runnableToast = new Java.Lang.Runnable(() =>
            {
                Toast.MakeText(Android.App.Application.Context, mensaje, ToastLength.Short).Show();
            });

            mainHandler.Post(runnableToast);
        }
    }
}