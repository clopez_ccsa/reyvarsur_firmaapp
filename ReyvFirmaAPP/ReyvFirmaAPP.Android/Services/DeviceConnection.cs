﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ReyvFirmaAPP.Services;
using ReyvFirmaAPP.Droid.Services;
using Xamarin.Forms;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Net;

[assembly: Dependency(typeof(DeviceConnection))]
namespace ReyvFirmaAPP.Droid.Services
{
    public class DeviceConnection : IDeviceConnection
    {
        public bool IsConnected()
        {
            ConnectivityManager connectivityManager = (ConnectivityManager)Android.App.Application.Context.GetSystemService(Context.ConnectivityService);
            NetworkInfo networkInfo = connectivityManager.ActiveNetworkInfo;
            bool isOnline = networkInfo.IsConnected;

            //bool isWifi = networkInfo.Type == ConnectivityType.Wifi;

            return isOnline;
        }
    }
}