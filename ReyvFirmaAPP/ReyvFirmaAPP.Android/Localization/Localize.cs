﻿using Xamarin.Forms;
using ReyvFirmaAPP.Localization;
using ReyvFirmaAPP.Droid.Localization;
using System.Globalization;

[assembly: Dependency(typeof(Localize))]

namespace ReyvFirmaAPP.Droid.Localization
{
    public class Localize : ILocalize
    {
        public CultureInfo GetCurrentCultureInfo()
        {
            var androidLocale = Java.Util.Locale.Default;
            var netLanguage = androidLocale.ToString().Replace("_", "-"); // turns pt_BR into pt-BR
            return new CultureInfo(netLanguage);
        }
    }
}