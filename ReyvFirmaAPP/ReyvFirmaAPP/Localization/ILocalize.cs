﻿using System.Globalization;

namespace ReyvFirmaAPP.Localization
{
    public interface ILocalize
    {
        CultureInfo GetCurrentCultureInfo();
    }
}
