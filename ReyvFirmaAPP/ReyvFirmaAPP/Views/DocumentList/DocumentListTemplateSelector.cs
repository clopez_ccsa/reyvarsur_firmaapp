﻿using ReyvFirmaAPP.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using static ReyvFirmaAPP.Models.DocumentTypeModel;

namespace ReyvFirmaAPP.Views.DocumentList
{
    public class DocumentListTemplateSelector : DataTemplateSelector
    {
        public DataTemplate BaseDocumentTemplate { get; set; }
        public DataTemplate DeliveryDocumentTemplate { get; set; }
        public DataTemplate InventoryTransferDocumentTemplate { get; set; }
        public DataTemplate VisitorDocumentTemplate { get; set; }
        public DataTemplate EpisDocumentTemplate { get; set; }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            DocumentDataModel model = (DocumentDataModel)item;
            switch (model.docType)
            {
                case (int)DocumentTypeEnum.Albaran:
                    return DeliveryDocumentTemplate;
                case (int)DocumentTypeEnum.Traslados:
                    return InventoryTransferDocumentTemplate;
                case (int)DocumentTypeEnum.Visitas:
                    return VisitorDocumentTemplate;
                case (int)DocumentTypeEnum.Epis:
                    return EpisDocumentTemplate;
                default:
                    return BaseDocumentTemplate;
            }
        }
    }
}
