﻿using Newtonsoft.Json;

namespace ReyvFirmaAPP.Models
{
    public class UserLoginModel : BaseModel
    {
        [JsonRequired]
        public string userName { get; set; }
        [JsonRequired]
        //[DataType(DataType.Password)]
        public string password { get; set; }
        [JsonRequired]
        public string token { get; set; }

        public string userFirstName { get; set; }
        public string userLastName { get; set; }
        public int idEmpleadoVentas { get; set; }

        //[JsonConstructor]
        public UserLoginModel(string _userName, string _password, string _token)
        {
            userName = _userName;
            password = _password;
            token = _token;
        }


        [JsonConstructor]
        public UserLoginModel(string _userName, string _password, string _token,
                              string _userFirstName, string _userLastName, int _idEmpleadoVentas)
        {
            userName = _userName;
            password = _password;
            token = _token;
            userFirstName = _userFirstName;
            userLastName = _userLastName;
            idEmpleadoVentas = _idEmpleadoVentas;
        }


        public string lastFirstName ()
        {
            return userLastName + ", " + userFirstName;
        }

        public string firstLastName ()
        {
            return userFirstName + " " + userLastName;
        }
    }
}
