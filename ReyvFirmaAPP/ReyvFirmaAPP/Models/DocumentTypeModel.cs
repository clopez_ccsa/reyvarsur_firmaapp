﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReyvFirmaAPP.Models
{
    public class DocumentTypeModel : BaseModel
    {
        public enum DocumentTypeEnum { Ninguno, Albaran, Traslados, Visitas, Epis, Envios, Vacaciones }
        private int _DocumentType { get; set; }
        private string _IconActive { get; set; }
        private string _IconInactive { get; set; }

        #region OnPropertyChanged properties code ***********************
        public int DocumentType
        {
            get { return _DocumentType; }
            set
            {
                if (_DocumentType != value)
                {
                    _DocumentType = value;
                    OnPropertyChanged(() => DocumentType);
                }
            }
        }

        public string IconActive
        {
            get { return _IconActive; }
            set
            {
                if (_IconActive != value)
                {
                    _IconActive = value;
                    OnPropertyChanged(() => IconActive);
                }
            }
        }

        public string IconInactive
        {
            get { return _IconInactive; }
            set
            {
                if (_IconInactive != value)
                {
                    _IconInactive = value;
                    OnPropertyChanged(() => IconInactive);
                }
            }
        }
        #endregion ******************************************************
    }
}
