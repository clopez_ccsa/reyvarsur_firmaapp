﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReyvFirmaAPP.Models
{
    public class DocumentDataModel : BaseModel
    {
        private int _docType { get; set; }
        private string _docNum { get; set; }
        private string _cardCode { get; set; }
        private string _cardName { get; set; }
        private DateTime _docDate { get; set; }
        private int _nItems { get; set; }

        #region OnPropertyChanged properties code ***********************
        public int docType
        {
            get { return _docType; }
            set
            {
                if (_docType == value) return;
                _docType = value;
                OnPropertyChanged(() => docType);
            }
        }

        public string docNum
        {
            get { return _docNum; }
            set
            {
                if (_docNum == value) return;
                _docNum = value;
                OnPropertyChanged(() => docNum);
            }
        }

        public string cardCode
        {
            get { return _cardCode; }
            set
            {
                if (_cardCode == value) return;
                _cardCode = value;
                OnPropertyChanged(() => cardCode);
            }
        }

        public string cardName
        {
            get { return _cardName; }
            set
            {
                if (_cardName == value) return;
                _cardName = value;
                OnPropertyChanged(() => cardName);
            }
        }

        public DateTime docDate
        {
            get { return _docDate; }
            set
            {
                if (_docDate == value) return;
                _docDate = value;
                OnPropertyChanged(() => docDate);
            }
        }

        public int nItems
        {
            get { return _nItems; }
            set
            {
                if (_nItems == value) return;
                _nItems = value;
                OnPropertyChanged(() => nItems);
            }
        }
        #endregion ******************************************************
    }

    public class DocumentLineDataModel : BaseModel
    {
        private double _quantity { get; set; }

        #region OnPropertyChanged properties code ***********************
        public double quantity
        {
            get { return _quantity; }
            set
            {
                if (_quantity == value) return;
                _quantity = value;
                OnPropertyChanged(() => quantity);
            }
        }
        #endregion ******************************************************
    }

    #region Derivative base data models ********************************************
    /// <summary>
    /// Añade información del firmante del documento al mismo
    /// </summary>
    public class DocumentSignerDataModel : DocumentDataModel
    {
        private string _firmaNombre { get; set; }
        private string _firmaApellidos { get; set; }
        private string _firmaNIF { get; set; }
        private string _firmaMatricula { get; set; }

        #region OnPropertyChanged properties code ***********************
        public string firmaNombre
        {
            get { return _firmaNombre; }
            set
            {
                if (_firmaNombre == value) return;
                _firmaNombre = value;
                OnPropertyChanged(() => firmaNombre);
            }
        }

        public string firmaApellidos
        {
            get { return _firmaApellidos; }
            set
            {
                if (_firmaApellidos == value) return;
                _firmaApellidos = value;
                OnPropertyChanged(() => firmaApellidos);
            }
        }

        public string firmaNIF
        {
            get { return _firmaNIF; }
            set
            {
                if (_firmaNIF == value) return;
                _firmaNIF = value;
                OnPropertyChanged(() => firmaNIF);
            }
        }

        public string firmaMatricula
        {
            get { return _firmaMatricula; }
            set
            {
                if (_firmaMatricula == value) return;
                _firmaMatricula = value;
                OnPropertyChanged(() => firmaMatricula);
            }
        }
        #endregion ******************************************************

        public string ApellidosNombre
        {
            get { return _firmaApellidos + ", " + _firmaNombre; }
        }

        public string FullData
        {
            get { return _firmaApellidos + ", " + _firmaNombre + Environment.NewLine + _firmaNIF + " - " + _firmaMatricula; }
        }
    }
    #endregion *********************************************************************




    #region Specific document data *************************************************
    //Albarán de entrega (Delivery)
    public class DeliveryDataModel : DocumentSignerDataModel
    {
        private double _docTotal { get; set; }

        #region OnPropertyChanged properties code (Delivery) ************
        public double docTotal
        {
            get { return _docTotal; }
            set
            {
                if (_docTotal == value) return;
                _docTotal = value;
                OnPropertyChanged(() => docTotal);
            }
        }
        #endregion ******************************************************
    }

    //Traspasos
    public class InventoryTransferDataModel : DocumentDataModel
    {
        private string _almacenOrigID { get; set; }
        private string _almacenOrig { get; set; }
        private string _almacenDestID { get; set; }
        private string _almacenDest { get; set; }

        #region OnPropertyChanged properties code (Inventory transfer) **
        public string almacenOrigID
        {
            get { return _almacenOrigID; }
            set
            {
                if (_almacenOrigID == value) return;
                _almacenOrigID = value;
                OnPropertyChanged(() => almacenOrigID);
            }
        }

        public string almacenOrig
        {
            get { return _almacenOrig; }
            set
            {
                if (_almacenOrig == value) return;
                _almacenOrig = value;
                OnPropertyChanged(() => almacenOrig);
            }
        }

        public string almacenDestID
        {
            get { return _almacenDestID; }
            set
            {
                if (_almacenDestID == value) return;
                _almacenDestID = value;
                OnPropertyChanged(() => almacenDestID);
            }
        }

        public string almacenDest
        {
            get { return _almacenDest; }
            set
            {
                if (_almacenDest == value) return;
                _almacenDest = value;
                OnPropertyChanged(() => almacenDest);
            }
        }
        #endregion ******************************************************
    }

    //Control de visitas
    public class VisitorDataModel : DocumentDataModel
    {
        //Se usa CardCode and CardName como el nombre de la visita
        private string _visitorId { get; set; }
        private string _visitorName { get; set; }
        private string _inChargeId { get; set; }
        private string _inChargeName { get; set; }
        private string _dni { get; set; }
        private DateTime _timeIn { get; set; }
        private DateTime _timeOut { get; set; }

        #region OnPropertyChanged properties code ***********************
        public string visitorId
        {
            get { return _visitorId; }
            set
            {
                if (_visitorId == value) return;
                _visitorId = value;
                OnPropertyChanged(() => visitorId);
            }
        }

        public string visitorName
        {
            get { return _visitorName; }
            set
            {
                if (_visitorName == value) return;
                _visitorName = value;
                OnPropertyChanged(() => visitorName);
            }
        }

        public string inChargeId
        {
            get { return _inChargeId; }
            set
            {
                if (_inChargeId == value) return;
                _inChargeId = value;
                OnPropertyChanged(() => inChargeId);
            }
        }

        public string inChargeName
        {
            get { return _inChargeName; }
            set
            {
                if (_inChargeName == value) return;
                _inChargeName = value;
                OnPropertyChanged(() => inChargeName);
            }
        }

        public string dni
        {
            get { return _dni; }
            set
            {
                if (_dni == value) return;
                _dni = value;
                OnPropertyChanged(() => dni);
            }
        }

        public DateTime timeIn
        {
            get { return _timeIn; }
            set
            {
                if (_timeIn == value) return;
                _timeIn = value;
                OnPropertyChanged(() => timeIn);
            }
        }

        public DateTime timeOut
        {
            get { return _timeOut; }
            set
            {
                if (_timeOut == value) return;
                _timeOut = value;
                OnPropertyChanged(() => timeOut);
            }
        }
        #endregion ******************************************************
    }

    //Entrega de EPIS
    public class EpisDataModel : DocumentDataModel
    {
        private List<DocumentLineDataModel> _itemList { get; set; }

        #region OnPropertyChanged properties code ***********************
        public List<DocumentLineDataModel> itemList
        {
            get { return _itemList; }
            set
            {
                if (_itemList == value) return;
                _itemList = value;
                OnPropertyChanged(() => itemList);
            }
        }
        #endregion ******************************************************
    }

    #endregion *********************************************************************
}
