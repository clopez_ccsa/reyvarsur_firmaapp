﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;

namespace ReyvFirmaAPP.Models
{
    // The model class files are shared between the mobile and service projects. 
    // If ITableData were compatible with PCL profile 78, the models could be in a PCL.

    public class BaseModel : INotifyPropertyChanged, INotifyCollectionChanged
    {
        private string _Id;
        private string _Name;
        private DateTimeOffset? _CreatedAt;
        private DateTimeOffset? _UpdatedAt;
        private bool _Deleted;
        private bool _Selected = false;
        private byte[] _Version;

        #region OnPropertyChanged properties code ***********************
        public string Id
        {
            get { return _Id; }
            set
            {
                if (_Id == value) return;
                _Id = value;
                OnPropertyChanged(() => Id);
            }
        }

        public string Name
        {
            get { return _Name; }
            set
            {
                if (_Name == value) return;
                _Name = value;
                OnPropertyChanged(() => Name);
            }
        }

        public DateTimeOffset? CreatedAt
        {
            get { return _CreatedAt; }
            set
            {
                if (_CreatedAt == value) return;
                _CreatedAt = value;
                OnPropertyChanged(() => CreatedAt);
            }
        }

        public DateTimeOffset? UpdatedAt
        {
            get { return _UpdatedAt; }
            set
            {
                if (_UpdatedAt == value) return;
                _UpdatedAt = value;
                OnPropertyChanged(() => UpdatedAt);
            }
        }

        public bool Deleted
        {
            get { return _Deleted; }
            set
            {
                if (_Deleted == value) return;
                _Deleted = value;
                OnPropertyChanged(() => Deleted);
            }
        }

        public bool Selected
        {
            get { return _Selected; }
            set
            {
                if (_Selected == value) return;
                _Selected = value;
                OnPropertyChanged(() => Selected);
            }
        }

        public byte[] Version
        {
            get { return _Version; }
            set
            {
                if (_Version == value) return;
                _Version = value;
                OnPropertyChanged(() => Version);
            }
        }
        #endregion ******************************************************

        public event PropertyChangedEventHandler PropertyChanged;
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        protected virtual void OnPropertyChanged<T>(Expression<Func<T>> e)
        {
            try
            {
                PropertyChangedEventHandler handler = this.PropertyChanged;
                if (handler == null) return;

                //The fully qualified property is really "ProjectName".Models.SomeModel.SomeProperty
                //so we need to strip that down just to the property name
                var ts = e.ToString();
                var lineage = ts.Split('.');
                if (!lineage.Any()) return;

                var name = lineage[lineage.Length - 1];
                if (name == null) return;

                handler?.Invoke(this, new PropertyChangedEventArgs(name));
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(" ****** EXCEPTION BASE MODEL!!!!!" + ex.Message + Environment.NewLine + ex.StackTrace);
            }
        }

        protected virtual void OnCollectionChanged<T>(Expression<Func<T>> e)
        {
            var handler = this.CollectionChanged;
            handler?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }
    }
}