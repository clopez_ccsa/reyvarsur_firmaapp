﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ReyvFirmaAPP.Models
{
    public class DocumentSignatureSendModel : BaseModel
    {
        private int _docType { get; set; }
        private string _docEntry { get; set; }
        private string _userCode { get; set; }
        private bool _sendByMail { get; set; }
        private byte[] _signature { get; set; }

        //Datos del firmante
        private string _firmaNombre { get; set; }
        private string _firmaApellidos { get; set; }
        private string _firmaNIF { get; set; }
        private string _firmaMatricula { get; set; }

        #region OnPropertyChanged properties code ***********************
        public int docType
        {
            get { return _docType; }
            set
            {
                if (_docType == value) return;
                _docType = value;
                OnPropertyChanged(() => docType);
            }
        }

        public string docEntry
        {
            get { return _docEntry; }
            set
            {
                if (_docEntry == value) return;
                _docEntry = value;
                OnPropertyChanged(() => docEntry);
            }
        }

        public string userCode
        {
            get { return _userCode; }
            set
            {
                if (_userCode == value) return;
                _userCode = value;
                OnPropertyChanged(() => userCode);
            }
        }

        public bool sendByMail
        {
            get { return _sendByMail; }
            set
            {
                if (_sendByMail == value) return;
                _sendByMail = value;
                OnPropertyChanged(() => sendByMail);
            }
        }

        public byte[] signature
        {
            get { return _signature; }
            set
            {
                if (_signature == value) return;
                _signature = value;
                OnPropertyChanged(() => signature);
            }
        }
        #endregion ****************************************************

        #region OnPropertyChanged firmante properties code **************
        public string firmaNombre
        {
            get { return _firmaNombre; }
            set
            {
                if (_firmaNombre == value) return;
                _firmaNombre = value;
                OnPropertyChanged(() => firmaNombre);
            }
        }

        public string firmaApellidos
        {
            get { return _firmaApellidos; }
            set
            {
                if (_firmaApellidos == value) return;
                _firmaApellidos = value;
                OnPropertyChanged(() => firmaApellidos);
            }
        }

        public string firmaNIF
        {
            get { return _firmaNIF; }
            set
            {
                if (_firmaNIF == value) return;
                _firmaNIF = value;
                OnPropertyChanged(() => firmaNIF);
            }
        }

        public string firmaMatricula
        {
            get { return _firmaMatricula; }
            set
            {
                if (_firmaMatricula == value) return;
                _firmaMatricula = value;
                OnPropertyChanged(() => firmaMatricula);
            }
        }
        #endregion ******************************************************
    }

}
