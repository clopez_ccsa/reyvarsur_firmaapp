using ReyvFirmaAPP.Localization;
using ReyvFirmaAPP.Pages.Base;
using ReyvFirmaAPP.Pages.MainMenu;
using ReyvFirmaAPP.Pages.Splash;
using ReyvFirmaAPP.Services;
using ReyvFirmaAPP.ViewModels.MainMenu;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation (XamlCompilationOptions.Compile)]
namespace ReyvFirmaAPP
{
	public partial class App : Application
	{
        static Application app;
        public static Application CurrentApp
        {
            get { return app; }
        }

        public App()
        {
            InitializeComponent();

            app = this;
            TextResources.Culture = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();

            if (!WebApiUserLoginData.isAuthenticated)
                MainPage = new SplashPage();
            else
                GoToMainPage();
        }

        public static void GoToMainPage()
        {
            /*CurrentApp.MainPage = new Pages.MainMenu.MainMenuPage()
            {
                //BindingContext = new MainMenuViewModel() { Navigation = this.Navigation}
                BindingContext = new MainMenuViewModel()
            };*/

            MainMenuPage mmPage = new MainMenuPage();
            CRMNavigationPage navPage = new CRMNavigationPage(mmPage);

            CurrentApp.MainPage = navPage;
            mmPage.BindingContext = new MainMenuViewModel() { Navigation = navPage.Navigation };
        }

        protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}

        public static async Task ExecuteIfConnected(Func<Task> actionToExecuteIfConnected)
        {
            if (IsConnected)
                await actionToExecuteIfConnected();
            else
                await DeviceMessages.ShowAlert(
                    TextResources.NetworkConnection_Alert_Title,
                    TextResources.NetworkConnection_Alert_Message,
                    TextResources.NetworkConnection_Alert_Confirm);
        }

        public static bool IsConnected
        {
            get { return DependencyService.Get<IDeviceConnection>().IsConnected(); }
        }
    }
}
