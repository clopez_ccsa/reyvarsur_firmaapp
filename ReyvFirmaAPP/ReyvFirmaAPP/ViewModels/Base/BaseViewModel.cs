﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using Xamarin.Forms;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Linq;

namespace ReyvFirmaAPP.ViewModels.Base
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        public INavigation Navigation { get; set; }

        public BaseViewModel(INavigation navigation = null)
        {
            Navigation = navigation;
        }

        public bool IsInitialized { get; set; }

        public async Task PushModalAsync(Page page)
        {
            if (Navigation != null)
                await Navigation.PushModalAsync(page);
        }

        public async Task PopModalAsync()
        {
            if (Navigation != null)
                await Navigation.PopModalAsync();
        }

        public async Task PushAsync(Page page)
        {
            if (Navigation != null)
                await Navigation.PushAsync(page);
        }

        public async Task PopAsync()
        {
            if (Navigation != null)
                await Navigation.PopAsync();
        }

        bool canLoadMore;
        /// <summary>
        /// Gets or sets the "IsBusy" property
        /// </summary>
        /// <value>The isbusy property.</value>
        public const string CanLoadMorePropertyName = "CanLoadMore";

        public bool CanLoadMore
        {
            get { return canLoadMore; }
            set { SetProperty(ref canLoadMore, value, CanLoadMorePropertyName); }
        }

        /*bool isBusy;
        /// <summary>
        /// Gets or sets the "IsBusy" property
        /// </summary>
        /// <value>The isbusy property.</value>
        public const string IsBusyPropertyName = "IsBusy";

        public bool IsBusy
        {
            get { return isBusy; }
            //set { SetProperty(ref isBusy, value, IsBusyPropertyName); }
            set
            {
                isBusy = value;
                OnPropertyChanged(() => IsBusy);
            }
        }*/

        //New version of IsBusy for using with more than one thread. Use a counter and returns true if there is more than 0 threads busy
        int isBusy;
        /// <summary>
        /// Gets or sets the "IsBusy" property
        /// </summary>
        /// <value>The isbusy property.</value>
        public const string IsBusyPropertyName = "IsBusy";

        public bool IsBusy
        {
            get
            {
                if (isBusy > 0)
                    return true;
                else
                    return false;
            }
            //set { SetProperty(ref isBusy, value, IsBusyPropertyName); }
            set
            {
                if (value)
                    isBusy++;
                else
                    isBusy--;

                OnPropertyChanged(() => IsBusy);
            }
        }

        bool isRefreshingMe;
        /// <summary>
        /// Gets or sets the "IsRefreshingMe" property
        /// </summary>
        /// <value>The isbusy property.</value>
        public const string IsRefreshingMePropertyName = "IsRefreshingMe";

        public bool IsRefreshingMe
        {
            get { return isRefreshingMe; }
            //set { SetProperty(ref isRefreshingMe, value, IsRefreshingMePropertyName); }
            set
            {
                isRefreshingMe = value;
                OnPropertyChanged(() => IsRefreshingMe);
            }
        }

        string title = string.Empty;
        /// <summary>
        /// Gets or sets the "Title" property
        /// </summary>
        /// <value>The title.</value>
        public const string TitlePropertyName = "Title";

        public string Title
        {
            get { return title; }
            set { SetProperty(ref title, value, TitlePropertyName); }
        }

        string subTitle = string.Empty;
        /// <summary>
        /// Gets or sets the "Subtitle" property
        /// </summary>
        public const string SubtitlePropertyName = "Subtitle";

        public string Subtitle
        {
            get { return subTitle; }
            set { SetProperty(ref subTitle, value, SubtitlePropertyName); }
        }

        string icon = null;
        /// <summary>
        /// Gets or sets the "Icon" of the viewmodel
        /// </summary>
        public const string IconPropertyName = "Icon";

        public string Icon
        {
            get { return icon; }
            set { SetProperty(ref icon, value, IconPropertyName); }
        }

        protected void SetProperty<U>(
            ref U backingStore, U value,
            string propertyName,
            Action onChanged = null,
            Action<U> onChanging = null)
        {
            if (EqualityComparer<U>.Default.Equals(backingStore, value))
                return;

            if (onChanging != null)
                onChanging(value);

            OnPropertyChanging(propertyName);

            backingStore = value;

            if (onChanged != null)
                onChanged();

            OnPropertyChanged(() => propertyName);
        }

        #region INotifyPropertyChanging implementation

        public event Xamarin.Forms.PropertyChangingEventHandler PropertyChanging;

        #endregion

        public void OnPropertyChanging(string propertyName)
        {
            if (PropertyChanging == null)
                return;

            PropertyChanging(this, new Xamarin.Forms.PropertyChangingEventArgs(propertyName));
        }

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        protected virtual void OnPropertyChanged<T>(Expression<Func<T>> e)
        {
            try
            {
                PropertyChangedEventHandler handler = this.PropertyChanged;
                if (handler == null) return;

                //The fully qualified property is really "ProjectName".Models.SomeModel.SomeProperty
                //so we need to strip that down just to the property name
                var ts = e.ToString();
                var lineage = ts.Split('.');
                if (!lineage.Any()) return;

                var name = lineage[lineage.Length - 1];
                if (name == null) return;

                handler?.Invoke(this, new PropertyChangedEventArgs(name));
            }
            catch (Exception ex)
            {
                Services.DeviceMessages.ShowAlert(Localization.TextResources.Error, ex.Message, "OK");
            }
        }

        #region posible implementación de OnPropertyChange
        /*protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (propertyName != null)
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public string SomeProperty
        {
            get { return _someProperty; }
            set { _someProperty = value; OnPropertyChanged(); }
        }
        // no need to add property name to OnPropertyChanged(), [CallerMemberName] takes care of that

        public void SomeMethod()
        {
            OnPropertyChanged(nameof(SomeProperty));
            // use nameof() to get a property name, no reflection is used (it gets injected during compile time into IL) = super fast.
        }*/
        #endregion

        #region código de comunicación con Web API
        const string apiURL = @"http://192.168.1.127:59214/api/";
        #endregion
    }
}