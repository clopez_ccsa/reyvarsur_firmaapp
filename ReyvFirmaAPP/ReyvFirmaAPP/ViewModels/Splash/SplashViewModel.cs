﻿using ReyvFirmaAPP.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using ReyvFirmaAPP.Services;
using static ReyvFirmaAPP.Statics.WebApiURLs;

namespace ReyvFirmaAPP.ViewModels.Splash
{
    public class SplashViewModel : BaseViewModel
    {
        readonly IConfigFetcher _ConfigFetcher;

        public SplashViewModel(INavigation navigation = null)
            : base(navigation)
        {
            _ConfigFetcher = DependencyService.Get<IConfigFetcher>();
            switch (clienteEnum)
            {
                case ClienteEnum.Reyvarsur: LogoCliente = "logo"; break;
                case ClienteEnum.Reduymeca: LogoCliente = "logofus"; break;
                case ClienteEnum.CCSA: LogoCliente = "logoccsa"; break;
            }
        }


        string _LogoCliente;

        public string LogoCliente
        {
            get { return _LogoCliente; }
            set
            {
                _LogoCliente = value;
                OnPropertyChanged(() => LogoCliente);
            }
        }

        string _Username;

        public string Username
        {
            get { return _Username; }
            set
            {
                _Username = value;
                OnPropertyChanged(() => Username);
            }
        }

        string _Password;

        public string Password
        {
            get { return _Password; }
            set
            {
                _Password = value;
                OnPropertyChanged(() => Password);
            }
        }

        string _Mensaje;

        public string Mensaje
        {
            get { return _Mensaje; }
            set
            {
                _Mensaje = value;
                OnPropertyChanged(() => Mensaje);
            }
        }
    }
}
