﻿using ReyvFirmaAPP.Models;
using ReyvFirmaAPP.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using static ReyvFirmaAPP.Statics.WebApiURLs;

namespace ReyvFirmaAPP.ViewModels.MainMenu
{
    public class MainMenuViewModel : BaseViewModel
    {
        public MainMenuViewModel ()
        {
            ObservableCollection<DocumentTypeModel> tempDocTypeList = new ObservableCollection<DocumentTypeModel>();

            /*tempDocTypeList.Add(new DocumentTypeModel { Id = "0", Name = "No documento", DocumentType = (int)DocumentTypeModel.DocumentTypeEnum.Ninguno,
                                                        Deleted = true, IconActive = "", IconInactive = "" });*/
            tempDocTypeList.Add(new DocumentTypeModel { Id = ((int)DocumentTypeModel.DocumentTypeEnum.Albaran).ToString(), Name = "Albarán de Venta",
                                                        DocumentType = (int)DocumentTypeModel.DocumentTypeEnum.Albaran,
                                                        Deleted = false, IconActive = "Albaran", IconInactive = "Albaran2" });
            tempDocTypeList.Add(new DocumentTypeModel { Id = ((int)DocumentTypeModel.DocumentTypeEnum.Traslados).ToString(), Name = "Traslados entre almacenes",
                                                        DocumentType = (int)DocumentTypeModel.DocumentTypeEnum.Traslados,
                                                        Deleted = false, IconActive = "Traslados", IconInactive = "Traslados2" });
            tempDocTypeList.Add(new DocumentTypeModel { Id = ((int)DocumentTypeModel.DocumentTypeEnum.Visitas).ToString(), Name = "Control de entrada",
                                                        DocumentType = (int)DocumentTypeModel.DocumentTypeEnum.Visitas,
                                                        Deleted = false, IconActive = "Visitas", IconInactive = "Visitas2" });
            tempDocTypeList.Add(new DocumentTypeModel { Id = ((int)DocumentTypeModel.DocumentTypeEnum.Epis).ToString(), Name = "Entrega de EPIS",
                                                        DocumentType = (int)DocumentTypeModel.DocumentTypeEnum.Epis,
                                                        Deleted = false, IconActive = "Epis", IconInactive = "Epis2" });
            tempDocTypeList.Add(new DocumentTypeModel { Id = ((int)DocumentTypeModel.DocumentTypeEnum.Envios).ToString(), Name = "Envío a la agencia de transporte",
                                                        DocumentType = (int)DocumentTypeModel.DocumentTypeEnum.Envios,
                                                        Deleted = true, IconActive = "Envios", IconInactive = "Envios2" });
            tempDocTypeList.Add(new DocumentTypeModel { Id = ((int)DocumentTypeModel.DocumentTypeEnum.Vacaciones).ToString(), Name = "Documento de vacaciones",
                                                        DocumentType = (int)DocumentTypeModel.DocumentTypeEnum.Vacaciones,
                                                        Deleted = true, IconActive = "Vacaciones", IconInactive = "Vacaciones2" });
            DocTypeList = tempDocTypeList;

            switch (clienteEnum)
            {
                case ClienteEnum.Reyvarsur: LogoCliente = "logo"; break;
                case ClienteEnum.Reduymeca: LogoCliente = "logofus"; break;
                case ClienteEnum.CCSA: LogoCliente = "logoccsa"; break;
            }
        }


        #region Data storage variables *************************************************
        private ObservableCollection<DocumentTypeModel> _DocTypeList;
        public ObservableCollection<DocumentTypeModel> DocTypeList
        {
            get { return _DocTypeList; }
            set
            {
                _DocTypeList = value;
                OnPropertyChanged(() => DocTypeList);
            }
        }

        private string _LogoCliente;
        public string LogoCliente
        {
            get { return _LogoCliente; }
            set
            {
                _LogoCliente = value;
                OnPropertyChanged(() => LogoCliente);
            }
        }
        #endregion *********************************************************************

        #region Navigation commands ****************************************************
        private Command _LoadDocumentCommand;
        public Command LoadDocumentCommand
        {
            get {  return _LoadDocumentCommand ?? (_LoadDocumentCommand = new Command (async(param) =>
                                                    {
                                                        if (param is DocumentTypeModel)
                                                            await ExecuteLoadDocument((DocumentTypeModel)param);
                                                    }
            )); }
        }

        private async Task ExecuteLoadDocument(DocumentTypeModel _docType)
        {
            await Navigation.PushAsync(new Pages.DocumentList.DocumentsPage()
                                        {
                                            Title = _docType.Name,
                                            BindingContext = new DocumentList.DocumentsViewModel((DocumentTypeModel.DocumentTypeEnum)_docType.DocumentType) { Navigation = this.Navigation }
                                        } );
        }
        #endregion *********************************************************************
    }
}
