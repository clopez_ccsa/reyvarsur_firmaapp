﻿using ReyvFirmaAPP.Localization;
using ReyvFirmaAPP.Services.WebApi;
using ReyvFirmaAPP.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ReyvFirmaAPP.ViewModels.ShowDBData
{
    public class ShowTextViewModel : BaseViewModel
    {

        public ShowTextViewModel (string _textUrl)
        {
            LoadTextFromBDCommand.Execute(_textUrl);
        }

        #region Data storage variables *************************************************************************
        private string _textShown;
        public string textShown
        {
            get { return _textShown; }
            set
            {
                if (_textShown != value)
                {
                    _textShown = value;
                    OnPropertyChanged(() => textShown);
                }
            }
        }
        #endregion ********************************************************************************************

        #region Load data commands ****************************************************************************
        private Command _LoadTextFromBDCommand;
        public Command LoadTextFromBDCommand
        {
            get { return _LoadTextFromBDCommand ?? (_LoadTextFromBDCommand = new Command(async (bdText) => await ExecuteLoadTextFromBDCommand((string)bdText))); }
        }

        private async Task ExecuteLoadTextFromBDCommand(string _bdText)
        {
            IsBusy = true;
            LoadTextFromBDCommand.ChangeCanExecute();

            textShown = await WebApiGetTextFromBDData.GetTextFromBd(_bdText);
            if (!string.IsNullOrWhiteSpace(textShown))
            {
                textShown = textShown.Replace("\r\n", Environment.NewLine);
                textShown = textShown.Replace("\n\r", Environment.NewLine);
                textShown = textShown.Replace("\r", Environment.NewLine);
                textShown = textShown.Replace("\n", Environment.NewLine);
            }
            else
            {
                textShown = TextResources.LOPD_Error;
            }

            IsBusy = false;
            LoadTextFromBDCommand.ChangeCanExecute();
        }
        #endregion ********************************************************************************************

        #region Navigation commands ***************************************************************************
        private Command _GoBackCommand;
        public Command GoBackCommand
        {
            get { return _GoBackCommand ?? (_GoBackCommand = new Command(ExecuteGoBack)); }
        }

        private void ExecuteGoBack()
        {
            Navigation.PopModalAsync();
        }
        #endregion ********************************************************************************************
    }
}
