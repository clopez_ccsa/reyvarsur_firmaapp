﻿using ReyvFirmaAPP.Localization;
using ReyvFirmaAPP.Models;
using ReyvFirmaAPP.Pages.DocumentList;
using ReyvFirmaAPP.Services;
using ReyvFirmaAPP.Services.WebApi;
using ReyvFirmaAPP.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using static ReyvFirmaAPP.Models.DocumentTypeModel;
using static ReyvFirmaAPP.Statics.Enums;

namespace ReyvFirmaAPP.ViewModels.DocumentList
{
    public class DocumentSignerDataViewModel : BaseViewModel
    {
        //***********************************************************************************************************
        //***********************************************************************************************************
        /// <summary>
        /// Clase para almacenar los datos de la lista de documentos
        /// </summary>
        public class DSCostumerData : BaseModel
        {
            private string _docList { get; set; }
            
            #region OnPropertyChanged properties code ***********************
            public string docList
            {
                get { return _docList; }
                set
                {
                    if (_docList == value) return;
                    _docList = value;
                    OnPropertyChanged(() => docList);
                }
            }
            #endregion ******************************************************
        }
        //***********************************************************************************************************
        //***********************************************************************************************************

        private DocumentTypeEnum docType;
        private int recienSeleccionado;

        public DocumentSignerDataViewModel(DocumentTypeEnum _docType, List<DocumentDataModel> _myDocuments)
        {
            DataConversion(_myDocuments);
            NombreEmpleadoVentas = Services.WebApiUserLoginData.loggedAccount.userName;
            docType = _docType;
            signerData = new DocumentSignerDataModel();
            autocompleteSelected = true;
            recienSeleccionado = 0;
        }

        #region nombre del empleado de ventas ******************************************************************
        private string _NombreEmpleadoVentas;
        public string NombreEmpleadoVentas
        {
            get { return _NombreEmpleadoVentas; }
            set { _NombreEmpleadoVentas = value; }
        }
        #endregion *********************************************************************************************

        #region Data storage variables *************************************************************************
        private List<DocumentDataModel> _documentList;
        public List<DocumentDataModel> documentList
        {
            get { return _documentList; }
            set
            {
                if (_documentList != value)
                {
                    _documentList = value;
                    OnPropertyChanged(() => documentList);
                }
            }
        }

        //Lista de clientes, con un resumen de los documentos a firmar. Incluye el ID del cliente,
        //el nombre, y una lista de números de documentos, separada por comas.
        private ObservableCollection<DSCostumerData> _costumerList;
        public ObservableCollection<DSCostumerData> costumerList
        {
            get { return _costumerList; }
            set
            {
                if (_costumerList != value)
                {
                    _costumerList = value;
                    OnPropertyChanged(() => costumerList);
                }
            }
        }


        private DocumentSignerDataModel _signerData;
        public DocumentSignerDataModel signerData
        {
            get { return _signerData; }
            set
            {
                if (_signerData != value)
                {
                    _signerData = value;
                    OnPropertyChanged(() => signerData);
                }
            }
        }


        private ObservableCollection<DocumentSignerDataModel> _signerAutocompletionList;
        public ObservableCollection<DocumentSignerDataModel> signerAutocompletionList
        {
            get { return _signerAutocompletionList; }
            set
            {
                if (_signerAutocompletionList != value)
                {
                    _signerAutocompletionList = value;
                    OnPropertyChanged(() => signerAutocompletionList);
                }
            }
        }

        //Indica si se ha seleccionado un elemento de la lista de autocompletar
        private bool _autocompleteSelected;
        public bool autocompleteSelected
        {
            get { return _autocompleteSelected; }
            set
            {
                if (_autocompleteSelected != value)
                {
                    _autocompleteSelected = value;
                    OnPropertyChanged(() => autocompleteSelected);
                }
            }
        }
        #endregion *********************************************************************************************



        #region Loading commands *******************************************************************************
        private Command _UpdateDocSigningInfoCommand;
        public Command UpdateDocSigningInfoCommand
        {
            get { return _UpdateDocSigningInfoCommand ?? (_UpdateDocSigningInfoCommand = new Command(async () => await ExecuteUpdateDocSigningInfo())); }
        }

        private async Task ExecuteUpdateDocSigningInfo()
        {
            List<DocumentSignerDataModel> tempList;

            //Nos aseguramos que esta búsqueda no sea porque acabamos de seleccionar un campo de autocompletar
            //Al ser 2 campos los que lanzan este comando (nombre y apellido) hay que contrarrestar dos búsquedas.
            if (recienSeleccionado > 0)
            {
                recienSeleccionado--;
                return;
            }


            if (IsBusy) return;

            IsBusy = true;
            UpdateDocSigningInfoCommand.ChangeCanExecute();

            if (!string.IsNullOrEmpty(signerData.firmaNombre) || !string.IsNullOrEmpty(signerData.firmaApellidos))
            {
                tempList = await WebApiDocumentsData.GetDocumentSignInfo((int)docType, signerData.firmaNombre, signerData.firmaApellidos);
                if (tempList != null)
                    signerAutocompletionList = new ObservableCollection<DocumentSignerDataModel>(tempList);
                else
                {
                    if (signerAutocompletionList == null)
                        signerAutocompletionList = new ObservableCollection<DocumentSignerDataModel>();
                    else
                        signerAutocompletionList.Clear();
                }
                autocompleteSelected = false;
            }
                

            UpdateDocSigningInfoCommand.ChangeCanExecute();
            IsBusy = false;
        }
        #endregion *********************************************************************************************

        #region Navigation commands ****************************************************************************
        private Command _GoBackCommand;
        public Command GoBackCommand
        {
            get { return _GoBackCommand ?? (_GoBackCommand = new Command(ExecuteGoBack)); }
        }

        private void ExecuteGoBack()
        {
            Navigation.PopAsync();
        }



        private Command _PushDocumentDetailsPageCommand;
        public Command PushDocumentDetailsPageCommand
        {
            get { return _PushDocumentDetailsPageCommand ?? (_PushDocumentDetailsPageCommand = new Command(async () => await ExecutePushDocumentDetailsPage())); }
        }

        private async Task ExecutePushDocumentDetailsPage()
        {
            string title;
            string errorMessage = null;

            switch (docType)
            {
                case DocumentTypeEnum.Albaran:
                    title = TextResources.DocumentsName_AlbaranesLista;
                    errorMessage = DataCheckAlbaran();
                    CopySignerToList(signerData, documentList);
                    break;
                case DocumentTypeEnum.Traslados:
                    title = TextResources.DocumentsName_Traslado + " " + documentList[0].docNum;
                    break;
                case DocumentTypeEnum.Visitas:
                    title = TextResources.DocumentsName_Visita + " " + documentList[0].docNum;
                    break;
                case DocumentTypeEnum.Epis:
                    title = TextResources.DocumentsName_Epis + " " + documentList[0].docDate.ToString("d", DependencyService.Get<ILocalize>().GetCurrentCultureInfo());
                    break;
                default:
                    title = "";
                    break;
            }

            if (string.IsNullOrWhiteSpace(errorMessage))
            {
                await Navigation.PushAsync(new DocumentDetailsPage()
                {
                    Title = title,
                    BindingContext = new DocumentDetailsViewModel(docType, documentList) { Navigation = this.Navigation }
                });
            }
            else
            {
                await DeviceMessages.ShowAlert(TextResources.General_Warning, errorMessage, "OK");
            }
        }
        #endregion *********************************************************************************************


        #region Form commands **********************************************************************************
        private Command _AutocompleteInfoCommand;
        public Command AutocompleteInfoCommand
        {
            get { return _AutocompleteInfoCommand ?? (_AutocompleteInfoCommand = new Command((param) => ExecuteAutocompleteInfo((DocumentSignerDataModel)param))); }
        }

        private void ExecuteAutocompleteInfo(DocumentSignerDataModel signerAutoInfo)
        {
            if (signerAutoInfo != null)
            {
                signerData.firmaNombre = signerAutoInfo.firmaNombre;
                signerData.firmaApellidos = signerAutoInfo.firmaApellidos;
                signerData.firmaNIF = signerAutoInfo.firmaNIF;
                signerData.firmaMatricula = signerAutoInfo.firmaMatricula;
                autocompleteSelected = true;
                recienSeleccionado = 2;
            }
        }
        #endregion *********************************************************************************************



        private void DataConversion (List<DocumentDataModel> _iniList)
        {
            //documentList = new List<DocumentSignerDataModel>();
            //costumerList = new List<DSCostumerData>();
            DocumentSignerDataModel tempDoc;
            DSCostumerData tempCostumer;

            if (_iniList != null)
            {
                //Inicialización de las listas
                if (documentList == null)
                    documentList = new List<DocumentDataModel>();
                if (documentList.Count > 0)
                    documentList.Clear();
                if (costumerList == null)
                    costumerList = new ObservableCollection<DSCostumerData>();
                if (costumerList.Count > 0)
                    costumerList.Clear();

                //Recorremos la lista de entrada convirtiendo los datos en la lista que se maneja en esta página y la siguiente(DocumentSignerDataModel)
                //y resumiendo los datos de los clientes
                for (int i = 0; i < _iniList.Count; i++)
                {
                    switch (_iniList[i].docType)
                    {
                        case (int)DocumentTypeModel.DocumentTypeEnum.Albaran:
                            tempDoc = new DeliveryDataModel();
                            ((DeliveryDataModel)tempDoc).docTotal = ((DeliveryDataModel)_iniList[i]).docTotal;
                            break;
                        default:
                            tempDoc = new DocumentSignerDataModel();
                            break;
                    }

                    tempDoc.Id = _iniList[i].Id;
                    tempDoc.docType = _iniList[i].docType;
                    tempDoc.docNum = _iniList[i].docNum;
                    tempDoc.cardCode = _iniList[i].cardCode;
                    tempDoc.cardName = _iniList[i].cardName;
                    tempDoc.docDate = _iniList[i].docDate;
                    tempDoc.nItems = _iniList[i].nItems;

                    documentList.Add(tempDoc);

                    tempCostumer = costumerList.FirstOrDefault(x => x.Id == tempDoc.cardCode);
                    if (tempCostumer == null)
                        costumerList.Add(new DSCostumerData()
                        {
                            Id = tempDoc.cardCode,
                            Name = tempDoc.cardName,
                            docList = tempDoc.docNum
                        });
                    else
                        tempCostumer.docList = tempCostumer.docList + ", " + tempDoc.docNum;
                }
            }
            
        }

        private void CopySignerToList (DocumentSignerDataModel _signer, List<DocumentDataModel> _list)
        {
            if ((_list != null) && (_list.Count > 0))
            {
                for (int i = 0; i < _list.Count; i++)
                {
                    ((DocumentSignerDataModel)_list[i]).firmaNombre = _signer.firmaNombre;
                    ((DocumentSignerDataModel)_list[i]).firmaApellidos = _signer.firmaApellidos;
                    ((DocumentSignerDataModel)_list[i]).firmaNIF = _signer.firmaNIF;
                    ((DocumentSignerDataModel)_list[i]).firmaMatricula = _signer.firmaMatricula;
                }
            }
        }


        private string DataCheckAlbaran ()
        {
            string dataChecked = "";

            if (string.IsNullOrWhiteSpace(signerData.firmaNombre) || string.IsNullOrWhiteSpace(signerData.firmaApellidos))
                dataChecked += Environment.NewLine + TextResources.DocumentSignerError_NoName;

            if (string.IsNullOrWhiteSpace(signerData.firmaNIF) && string.IsNullOrWhiteSpace(signerData.firmaMatricula))
                dataChecked += Environment.NewLine + TextResources.DocumentSignerError_NoNIFPlate;

            if (!string.IsNullOrWhiteSpace(dataChecked))
                dataChecked = TextResources.DocumentSignerError_Main + dataChecked;

            return dataChecked;
        }
    }
}
