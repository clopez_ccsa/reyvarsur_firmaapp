﻿using ReyvFirmaAPP.Localization;
using ReyvFirmaAPP.Models;
using ReyvFirmaAPP.Pages.DocumentList;
using ReyvFirmaAPP.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ReyvFirmaAPP.ViewModels.DocumentList
{
    public class DocumentLinesViewModel : BaseViewModel
    {
        public DocumentLinesViewModel(DocumentDataModel _myDocument)
        {
            document = _myDocument;
            NombreEmpleadoVentas = Services.WebApiUserLoginData.loggedAccount.userName;

            UpdateDocumentLines();
        }

        #region nombre del empleado de ventas ******************************************************************
        private string _NombreEmpleadoVentas;
        public string NombreEmpleadoVentas
        {
            get { return _NombreEmpleadoVentas; }
            set { _NombreEmpleadoVentas = value; }
        }
        #endregion *********************************************************************************************

        #region Data storage variables *************************************************************************
        private DocumentDataModel _document;
        public DocumentDataModel document
        {
            get { return _document; }
            set
            {
                if (_document != value)
                {
                    _document = value;
                    OnPropertyChanged(() => document);
                    UpdateDocumentLines();
                }
            }
        }

        private ObservableCollection<DocumentLineDataModel> _documentLines;
        public ObservableCollection<DocumentLineDataModel> documentLines
        {
            get { return _documentLines; }
            set
            {
                if (_documentLines != value)
                {
                    _documentLines = value;
                    OnPropertyChanged(() => documentLines);
                }
            }
        }
        #endregion *********************************************************************************************


        #region Navigation commands ****************************************************************************
        private Command _GoBackCommand;
        public Command GoBackCommand
        {
            get { return _GoBackCommand ?? (_GoBackCommand = new Command(ExecuteGoBack)); }
        }

        private void ExecuteGoBack()
        {
            Navigation.PopAsync();
        }


        private Command _PushDocumentDetailsPageCommand;
        public Command PushDocumentDetailsPageCommand
        {
            get { return _PushDocumentDetailsPageCommand ?? (_PushDocumentDetailsPageCommand = new Command(async () => await ExecutePushDocumentDetailsPage())); }
        }

        private async Task ExecutePushDocumentDetailsPage()
        {
            string title;

            switch (document.docType)
            {
                case (int)DocumentTypeModel.DocumentTypeEnum.Albaran:
                    title = TextResources.DocumentsName_Albaran + " " + document.docNum;
                    break;
                case (int)DocumentTypeModel.DocumentTypeEnum.Traslados:
                    title = TextResources.DocumentsName_Traslado + " " + document.docNum;
                    break;
                case (int)DocumentTypeModel.DocumentTypeEnum.Visitas:
                    title = TextResources.DocumentsName_Visita + " " + document.docNum;
                    break;
                case (int)DocumentTypeModel.DocumentTypeEnum.Epis:
                    title = TextResources.DocumentsName_Epis + " " + document.docDate.ToString("d", DependencyService.Get<ILocalize>().GetCurrentCultureInfo());
                    break;
                default:
                    title = "";
                    break;
            }

            await Navigation.PushAsync(new DocumentDetailsPage()
            {
                Title = title,
                BindingContext = new DocumentDetailsViewModel(document) { Navigation = this.Navigation }
            });
        }
        #endregion *********************************************************************************************


        private void UpdateDocumentLines()
        {
            switch (document.docType)
            {
                case (int)DocumentTypeModel.DocumentTypeEnum.Epis:
                    if (((EpisDataModel)document).itemList != null)
                        documentLines = new ObservableCollection<DocumentLineDataModel>(((EpisDataModel)document).itemList);
                    else
                        documentLines = new ObservableCollection<DocumentLineDataModel>();
                    break;
            }
        }
    }
}
