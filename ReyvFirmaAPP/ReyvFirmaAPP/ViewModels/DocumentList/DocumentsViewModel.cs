﻿using ReyvFirmaAPP.Localization;
using ReyvFirmaAPP.Models;
using ReyvFirmaAPP.Pages.DocumentList;
using ReyvFirmaAPP.Services.WebApi;
using ReyvFirmaAPP.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using static ReyvFirmaAPP.Models.DocumentTypeModel;
using static ReyvFirmaAPP.Statics.Enums;

namespace ReyvFirmaAPP.ViewModels.DocumentList
{
    public class DocumentsViewModel : BaseViewModel
    {
        private DocumentTypeEnum docType;

        public DocumentsViewModel(DocumentTypeEnum _docType)
        {
            docType = _docType;
            NombreEmpleadoVentas = Services.WebApiUserLoginData.loggedAccount.userName;
            //LoadDocumentListCommand.Execute(null);

            switch (docType)
            {
                case DocumentTypeEnum.Albaran:
                    multiSelection = true;
                    break;
                default:
                    multiSelection = false;
                    break;
            }
        }

        #region nombre del empleado de ventas ******************************************************************
        private string _NombreEmpleadoVentas;
        public string NombreEmpleadoVentas
        {
            get { return _NombreEmpleadoVentas; }
            set { _NombreEmpleadoVentas = value; }
        }
        #endregion *********************************************************************************************

        #region Other variables ********************************************************************************
        private bool _multiSelection;
        public bool multiSelection
        {
            get { return _multiSelection; }
            set { _multiSelection = value; }
        }

        private int _itemsSelected;
        public bool itemsSelected
        {
            get { return _itemsSelected > 0; }
            set
            {
                if (value)
                    _itemsSelected++;
                else
                {
                    if (_itemsSelected > 0)
                        _itemsSelected--;
                }
                OnPropertyChanged(() => itemsSelected);
            }
        }
        #endregion *********************************************************************************************

        
        #region Data storage variables *************************************************************************
        private ObservableCollection<DocumentDataModel> _DocumentList;
        public ObservableCollection<DocumentDataModel> DocumentList
        {
            get { return _DocumentList; }
            set
            {
                if (_DocumentList != value)
                {
                    _DocumentList = value;
                    OnPropertyChanged(() => DocumentList);
                }
            }
        }
        #endregion *********************************************************************************************

        #region Load data commands *****************************************
        private Command _LoadDocumentListCommand;
        public Command LoadDocumentListCommand
        {
            get { return _LoadDocumentListCommand ?? (_LoadDocumentListCommand = new Command(async () => await ExecuteLoadDocumentListCommand(docType))); }
        }

        private async Task ExecuteLoadDocumentListCommand(DocumentTypeEnum _docType)
        {
            IsBusy = true;
            LoadDocumentListCommand.ChangeCanExecute();

            object tempList = await WebApiDocumentsData.GetDocumentsList(Services.WebApiUserLoginData.loggedAccount.userName, (int)_docType);
            _itemsSelected = 0;
            itemsSelected = false;  //Para generar el evento que actualice el mostrar o no el botón de siguiente en caso de volver de una página posterior

            if (tempList != null)
            {
                DocumentList = new ObservableCollection<DocumentDataModel>(); //if tempList is null, it creates an exception
                switch (_docType)
                {
                    case DocumentTypeEnum.Albaran:
                        tempList = ((List<DeliveryDataModel>)tempList).OrderBy(x => x.cardName).ThenByDescending(x => x.docNum).ToList();
                        for (int i = 0; i < ((List<DeliveryDataModel>)tempList).Count; i++)
                            DocumentList.Add(((List<DeliveryDataModel>)tempList)[i]);
                        break;
                    case DocumentTypeEnum.Traslados:
                        for (int i = 0; i < ((List<InventoryTransferDataModel>)tempList).Count; i++)
                            DocumentList.Add(((List<InventoryTransferDataModel>)tempList)[i]);
                        break;
                    case DocumentTypeEnum.Visitas:
                        for (int i = 0; i < ((List<VisitorDataModel>)tempList).Count; i++)
                            DocumentList.Add(((List<VisitorDataModel>)tempList)[i]);
                        break;
                    case DocumentTypeEnum.Epis:
                        for (int i = 0; i < ((List<EpisDataModel>)tempList).Count; i++)
                            DocumentList.Add(((List<EpisDataModel>)tempList)[i]);
                        break;
                    default:
                        for (int i = 0; i < ((List<DocumentDataModel>)tempList).Count; i++)
                            DocumentList.Add(((List<DocumentDataModel>)tempList)[i]);
                        break;
                }
            }
            else
                if (DocumentList != null)
                DocumentList.Clear();

            IsBusy = false;
            LoadDocumentListCommand.ChangeCanExecute();
        }


        private Command _RefreshDocumentListCommand;
        public Command RefreshDocumentListCommand
        {
            get { return _RefreshDocumentListCommand ?? (_RefreshDocumentListCommand = new Command(async () => await ExecuteRefreshDocumentListCommand(docType))); }
        }

        private async Task ExecuteRefreshDocumentListCommand(DocumentTypeEnum _docType)
        {
            IsRefreshingMe = true;
            RefreshDocumentListCommand.ChangeCanExecute();

            object tempList = await WebApiDocumentsData.GetDocumentsList(Services.WebApiUserLoginData.loggedAccount.userName, (int)_docType);
            _itemsSelected = 0;
            itemsSelected = false;  //Para generar el evento que actualice el mostrar o no el botón de siguiente en caso de volver de una página posterior

            if (tempList != null)
            {
                DocumentList = new ObservableCollection<DocumentDataModel>(); //if tempList is null, it creates an exception
                switch (_docType)
                {
                    case DocumentTypeEnum.Albaran:
                        tempList = ((List<DeliveryDataModel>)tempList).OrderBy(x => x.cardName).ThenByDescending(x => x.docNum).ToList();
                        for (int i = 0; i < ((List<DeliveryDataModel>)tempList).Count; i++)
                            DocumentList.Add(((List<DeliveryDataModel>)tempList)[i]);
                        break;
                    case DocumentTypeEnum.Traslados:
                        for (int i = 0; i < ((List<InventoryTransferDataModel>)tempList).Count; i++)
                            DocumentList.Add(((List<InventoryTransferDataModel>)tempList)[i]);
                        break;
                    case DocumentTypeEnum.Visitas:
                        for (int i = 0; i < ((List<VisitorDataModel>)tempList).Count; i++)
                            DocumentList.Add(((List<VisitorDataModel>)tempList)[i]);
                        break;
                    case DocumentTypeEnum.Epis:
                        for (int i = 0; i < ((List<EpisDataModel>)tempList).Count; i++)
                            DocumentList.Add(((List<EpisDataModel>)tempList)[i]);
                        break;
                    default:
                        for (int i = 0; i < ((List<DocumentDataModel>)tempList).Count; i++)
                            DocumentList.Add(((List<DocumentDataModel>)tempList)[i]);
                        break;
                }
            }
            else
                if (DocumentList != null)
                DocumentList.Clear();

            IsRefreshingMe = false;
            RefreshDocumentListCommand.ChangeCanExecute();
        }
        #endregion *********************************************************


        #region Load lines data commands ***********************************
        private Command _LoadDocumentLinesCommand;
        public Command LoadDocumentLinesCommand
        {
            get { return _LoadDocumentLinesCommand ?? (_LoadDocumentLinesCommand = new Command(async (_document) => await ExecuteLoadDocumentLinesCommand((DocumentDataModel)_document))); }
        }

        private async Task ExecuteLoadDocumentLinesCommand(DocumentDataModel _document)
        {
            List<DocumentLineDataModel> tempList;

            IsBusy = true;
            LoadDocumentListCommand.ChangeCanExecute();

            if (_document != null)
            {
                tempList = await WebApiDocumentsData.GetDocumentLines(_document.Id, _document.docType);
                /*tempList = new List<DocumentLineDataModel>();
                tempList.Add(new DocumentLineDataModel() { Id = "01", Name = "Producto01", quantity = 5 });
                tempList.Add(new DocumentLineDataModel() { Id = "02", Name = "Producto02", quantity = 6 });
                tempList.Add(new DocumentLineDataModel() { Id = "03", Name = "Producto03", quantity = 7 });
                tempList.Add(new DocumentLineDataModel() { Id = "04", Name = "Producto04 Y escribo y escribo y escribo para ver cómo queda un artículo con multiples líneas.", quantity = 8 });*/

                switch (_document.docType)
                {
                    case (int)DocumentTypeEnum.Epis:
                        ((EpisDataModel)_document).itemList = tempList;
                        break;
                }
            }
            else
            {
                switch (_document.docType)
                {
                    case (int)DocumentTypeEnum.Epis:
                        if (((EpisDataModel)_document).itemList != null)
                            ((EpisDataModel)_document).itemList.Clear();
                        break;
                }
            }

            IsBusy = false;
            LoadDocumentListCommand.ChangeCanExecute();
        }
        #endregion *********************************************************


        #region Navigation commands ****************************************
        private Command _PushDocumentDetailsPageCommand;
        public Command PushDocumentDetailsPageCommand
        {
            get { return _PushDocumentDetailsPageCommand ?? (_PushDocumentDetailsPageCommand = new Command(async (_document) => await ExecutePushDocumentDetailsPage((DocumentDataModel)_document))); }
        }

        private async Task ExecutePushDocumentDetailsPage(DocumentDataModel document)
        {
            string title;
            //bool showLines = false;
            NextPageEnum nextPage = NextPageEnum.DocumentDetails;

            switch (document.docType)
            {
                case (int)DocumentTypeModel.DocumentTypeEnum.Albaran:
                    title = TextResources.DocumentsName_Albaran + " " + document.docNum;
                    nextPage = NextPageEnum.SignerData;
                    break;
                case (int)DocumentTypeModel.DocumentTypeEnum.Traslados:
                    title = TextResources.DocumentsName_Traslado + " " + document.docNum;
                    break;
                case (int)DocumentTypeModel.DocumentTypeEnum.Visitas:
                    title = TextResources.DocumentsName_Visita + " " + document.docNum;
                    break;
                case (int)DocumentTypeModel.DocumentTypeEnum.Epis:
                    title = TextResources.DocumentsName_Epis + " " + document.docDate.ToString("d", DependencyService.Get<ILocalize>().GetCurrentCultureInfo());
                    //LoadDocumentLinesCommand.Execute(document);
                    await ExecuteLoadDocumentLinesCommand(document);
                    nextPage = NextPageEnum.DocumentLines;
                    break;
                default:
                    title = "";
                    break;
            }

            switch (nextPage) {
                case NextPageEnum.DocumentLines:
                    await Navigation.PushAsync(new DocumentLinesPage()
                    {
                        Title = title,
                        BindingContext = new DocumentLinesViewModel(document) { Navigation = this.Navigation }
                    });
                    break;
                case NextPageEnum.SignerData:
                    await Navigation.PushAsync(new DocumentSignerDataPage()
                    {
                        Title = title,
                        BindingContext = new DocumentSignerDataViewModel((DocumentTypeEnum)document.docType, new List<DocumentDataModel>() { document }) { Navigation = this.Navigation }
                    });
                    break;
                default:
                    await Navigation.PushAsync(new DocumentDetailsPage()
                    {
                        Title = title,
                        BindingContext = new DocumentDetailsViewModel(document) { Navigation = this.Navigation }
                    });
                    break;
            }
            
        }



        private Command _PushDocumentListPageCommand;
        public Command PushDocumentListPageCommand
        {
            get { return _PushDocumentListPageCommand ?? (_PushDocumentListPageCommand = new Command(async () => await ExecutePushDocumentListPage())); }
        }

        private async Task ExecutePushDocumentListPage()
        {
            if (IsBusy) return;

            IsBusy = true;
            PushDocumentListPageCommand.ChangeCanExecute();

            List<DocumentDataModel> filteredList = DocumentList.Where(x => x.Selected).ToList();

            if ((filteredList != null) && (filteredList.Count > 0))
            {
                await Navigation.PushAsync(new DocumentListConfirmationPage()
                {
                    Title = TextResources.DocumentConfirmationPage_Title,
                    BindingContext = new DocumentListConfirmationViewModel(docType, filteredList) { Navigation = this.Navigation }
                });
            }

            PushDocumentListPageCommand.ChangeCanExecute();
            IsBusy = false;
        }
        #endregion *********************************************************




        #region nombre del empleado de ventas ******************************************************************
        /*private Command _LoadDocumentDetailsPage;
        public Command LoadDocumentDetailsPage
        {
            get { return _LoadDocumentDetailsPage ?? (_LoadDocumentDetailsPage = new Command(async (_doc) => {
                if (_doc is DocumentDataModel)
                    await ExecuteLoadDocumentDetailsPage((DocumentDataModel)_doc);
                    })); }
        }

        private async Task ExecuteLoadDocumentDetailsPage(DocumentDataModel _doc)
        {
            await Navigation.PushAsync(new DocumentDetailsPage() { BindingContext = new DocumentDetailsViewModel(_doc, this.Navigation) { Navigation = this.Navigation } });
        }*/
        #endregion *********************************************************************************************


        #region nombre del empleado de ventas ******************************************************************
        private Command _SelectUnselectDocumentCommand;
        public Command SelectUnselectDocumentCommand
        {
            get { return _SelectUnselectDocumentCommand ?? (_SelectUnselectDocumentCommand = new Command( (_doc) => {
                if (_doc is DocumentDataModel)
                    ExecuteSelectUnselectDocument((DocumentDataModel)_doc);
                    })); }
        }

        private void ExecuteSelectUnselectDocument(DocumentDataModel _doc)
        {
            _doc.Selected = !_doc.Selected;
            itemsSelected = _doc.Selected;
        }
        #endregion *********************************************************************************************
    }
}
