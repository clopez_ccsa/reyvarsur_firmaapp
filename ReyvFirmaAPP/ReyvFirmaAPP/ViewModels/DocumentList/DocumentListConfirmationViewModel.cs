﻿using ReyvFirmaAPP.Localization;
using ReyvFirmaAPP.Models;
using ReyvFirmaAPP.Pages.DocumentList;
using ReyvFirmaAPP.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using static ReyvFirmaAPP.Models.DocumentTypeModel;
using static ReyvFirmaAPP.Statics.Enums;

namespace ReyvFirmaAPP.ViewModels.DocumentList
{
    public class DocumentListConfirmationViewModel : BaseViewModel
    {
        private DocumentTypeEnum docType;

        public DocumentListConfirmationViewModel (DocumentTypeEnum _docType, List<DocumentDataModel> _documentList)
        {
            docType = _docType;
            if ((_documentList != null) && (_documentList.Count > 0))
            {
                for (int i = 0; i < _documentList.Count; i++)
                    _documentList[i].Selected = false;
            }
            DocumentList = new ObservableCollection<DocumentDataModel> (_documentList);
            NombreEmpleadoVentas = Services.WebApiUserLoginData.loggedAccount.userName;
        }


        #region nombre del empleado de ventas ******************************************************************
        private string _NombreEmpleadoVentas;
        public string NombreEmpleadoVentas
        {
            get { return _NombreEmpleadoVentas; }
            set { _NombreEmpleadoVentas = value; }
        }
        #endregion *********************************************************************************************

        #region Data storage variables *************************************************************************
        private ObservableCollection<DocumentDataModel> _DocumentList;
        public ObservableCollection<DocumentDataModel> DocumentList
        {
            get { return _DocumentList; }
            set
            {
                if (_DocumentList != value)
                {
                    _DocumentList = value;
                    OnPropertyChanged(() => DocumentList);
                }
            }
        }
        #endregion *********************************************************************************************


        #region Navigation commands ****************************************
        private Command _GoBackCommand;
        public Command GoBackCommand
        {
            get { return _GoBackCommand ?? (_GoBackCommand = new Command(ExecuteGoBack)); }
        }

        private void ExecuteGoBack()
        {
            Navigation.PopAsync();
        }


        private Command _PushDocumentDetailsPageCommand;
        public Command PushDocumentDetailsPageCommand
        {
            get { return _PushDocumentDetailsPageCommand ?? (_PushDocumentDetailsPageCommand = new Command(async (_document) => await ExecutePushDocumentDetailsPage((DocumentDataModel)_document))); }
        }

        private async Task ExecutePushDocumentDetailsPage(DocumentDataModel document)
        {
            string title = "";
            //bool showLines = false;
            NextPageEnum nextPage = NextPageEnum.DocumentDetails;

            switch (docType)
            {
                case DocumentTypeEnum.Albaran:
                    title = TextResources.DocumentsName_AlbaranesLista;
                    nextPage = NextPageEnum.SignerData;
                    break;
                case DocumentTypeEnum.Traslados:
                    break;
                case DocumentTypeEnum.Visitas:
                    break;
                case DocumentTypeEnum.Epis:
                    break;
                default:
                    title = "";
                    break;
            }

            switch (nextPage)
            {
                case NextPageEnum.DocumentLines:
                    await Navigation.PushAsync(new DocumentLinesPage()
                    {
                        Title = title,
                        BindingContext = new DocumentLinesViewModel(document) { Navigation = this.Navigation }
                    });
                    break;
                case NextPageEnum.SignerData:
                    await Navigation.PushAsync(new DocumentSignerDataPage()
                    {
                        Title = title,
                        BindingContext = new DocumentSignerDataViewModel(docType, DocumentList.ToList()) { Navigation = this.Navigation }
                    });
                    break;
                default:
                    await Navigation.PushAsync(new DocumentDetailsPage()
                    {
                        Title = title,
                        BindingContext = new DocumentDetailsViewModel(document) { Navigation = this.Navigation }
                    });
                    break;
            }

        }
        #endregion *********************************************************
    }
}
