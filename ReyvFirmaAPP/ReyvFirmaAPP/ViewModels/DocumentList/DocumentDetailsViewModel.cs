﻿using ReyvFirmaAPP.Localization;
using ReyvFirmaAPP.Models;
using ReyvFirmaAPP.Pages.PDFPages;
using ReyvFirmaAPP.Pages.ShowDBData;
using ReyvFirmaAPP.Services;
using ReyvFirmaAPP.Services.WebApi;
using ReyvFirmaAPP.Statics;
using ReyvFirmaAPP.ViewModels.Base;
using ReyvFirmaAPP.ViewModels.ShowDBData;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ReyvFirmaAPP.ViewModels.DocumentList
{
    public class DocumentDetailsViewModel : BaseViewModel
    {
        DocumentTypeModel.DocumentTypeEnum docType;

        public DocumentDetailsViewModel (DocumentDataModel _myDocument)
        {
            multiDocumentMode = false;
            documentList = new List<DocumentDataModel>() { _myDocument };
            NombreEmpleadoVentas = Services.WebApiUserLoginData.loggedAccount.userName;
            signIsBlank = true;
            showHojaDeSeguridadButton = _myDocument.docType == (int)DocumentTypeModel.DocumentTypeEnum.Visitas;
            showLOPDButton = _myDocument.docType == (int)DocumentTypeModel.DocumentTypeEnum.Visitas;
            docType = (DocumentTypeModel.DocumentTypeEnum)_myDocument.docType;
        }

        public DocumentDetailsViewModel(DocumentTypeModel.DocumentTypeEnum _docType, List<DocumentDataModel> _myDocuments)
        {
            multiDocumentMode = true;
            documentList = _myDocuments;
            NombreEmpleadoVentas = Services.WebApiUserLoginData.loggedAccount.userName;
            signIsBlank = true;
            showHojaDeSeguridadButton = _docType == DocumentTypeModel.DocumentTypeEnum.Visitas;
            showLOPDButton = _docType == DocumentTypeModel.DocumentTypeEnum.Visitas;
            DataConversion(_myDocuments);
            docType = _docType;
        }

        #region nombre del empleado de ventas ******************************************************************
        private string _NombreEmpleadoVentas;
        public string NombreEmpleadoVentas
        {
            get { return _NombreEmpleadoVentas; }
            set { _NombreEmpleadoVentas = value; }
        }
        #endregion *********************************************************************************************

        #region Data storage variables *************************************
        private List<DocumentDataModel> _documentList;
        public List<DocumentDataModel> documentList
        {
            get { return _documentList; }
            set
            {
                if (_documentList != value)
                {
                    _documentList = value;
                    OnPropertyChanged(() => documentList);

                    if (multiDocumentMode)
                        OnPropertyChanged(() => listSignerSummaryData);
                }
            }
        }


        //Lista de clientes, con un resumen de los documentos a firmar. Incluye el ID del cliente,
        //el nombre, y una lista de números de documentos, separada por comas.
        private ObservableCollection<DocumentSignerDataViewModel.DSCostumerData> _costumerList;
        public ObservableCollection<DocumentSignerDataViewModel.DSCostumerData> costumerList
        {
            get { return _costumerList; }
            set
            {
                if (_costumerList != value)
                {
                    _costumerList = value;
                    OnPropertyChanged(() => costumerList);
                }
            }
        }
        #endregion *********************************************************

        #region Sending variables ********************************************
        private bool _sendingSigns;
        public bool sendingSigns
        {
            get { return _sendingSigns; }
            set
            {
                if (_sendingSigns != value)
                {
                    _sendingSigns = value;
                    OnPropertyChanged(() => sendingSigns);
                }
            }
        }

        private double _sendedPrcnt;
        public double sendedPrcnt
        {
            get { return _sendedPrcnt; }
            set
            {
                if (_sendedPrcnt != value)
                {
                    _sendedPrcnt = value;
                    OnPropertyChanged(() => sendedPrcnt);
                }
            }
        }

        private int _nDocSending;
        public int nDocSending
        {
            get { return _nDocSending; }
            set
            {
                if (_nDocSending != value)
                {
                    _nDocSending = value;
                    OnPropertyChanged(() => nDocSending);
                }
            }
        }

        private int _totalDocSending;
        public int totalDocSending
        {
            get { return _totalDocSending; }
            set
            {
                if (_totalDocSending != value)
                {
                    _totalDocSending = value;
                    OnPropertyChanged(() => totalDocSending);
                }
            }
        }


        private string _nameDocSending;
        public string nameDocSending
        {
            get { return _nameDocSending; }
            set
            {
                if (_nameDocSending != value)
                {
                    _nameDocSending = value;
                    OnPropertyChanged(() => nameDocSending);
                }
            }
        }
        #endregion *********************************************************


        #region Other variables ********************************************
        private bool _multiDocumentMode;
        public bool multiDocumentMode
        {
            get { return _multiDocumentMode; }
            set
            {
                if (_multiDocumentMode != value)
                {
                    _multiDocumentMode = value;
                    OnPropertyChanged(() => multiDocumentMode);
                }
            }
        }


        private bool _signIsBlank;
        public bool signIsBlank
        {
            get { return _signIsBlank; }
            set
            {
                if (_signIsBlank != value)
                {
                    _signIsBlank = value;
                    OnPropertyChanged(() => signIsBlank);
                }
            }
        }

        private bool _showHojaDeSeguridadButton;
        public bool showHojaDeSeguridadButton
        {
            get { return _showHojaDeSeguridadButton; }
            set
            {
                if (_showHojaDeSeguridadButton != value)
                {
                    _showHojaDeSeguridadButton = value;
                    OnPropertyChanged(() => showHojaDeSeguridadButton);
                }
            }
        }

        private bool _showLOPDButton;
        public bool showLOPDButton
        {
            get { return _showLOPDButton; }
            set
            {
                if (_showLOPDButton != value)
                {
                    _showLOPDButton = value;
                    OnPropertyChanged(() => showLOPDButton);
                }
            }
        }


        public string listSignerSummaryData
        {
            get
            {
                if ((multiDocumentMode) && (documentList != null) && (documentList.Count > 0))
                {
                    DocumentSignerDataModel lssdDoc = (DocumentSignerDataModel)documentList[0];
                    return string.Format(" {0}, {1}     {2} - {3}", lssdDoc.firmaApellidos, lssdDoc.firmaNombre, lssdDoc.firmaNIF, lssdDoc.firmaMatricula);
                }
                else
                    return "";

            }
        }
        #endregion *********************************************************


        #region Other buttons Commands *************************************
        private Command _LoadSecuritySheetPDFCommand;
        public Command LoadSecuritySheetPDFCommand
        {
            get { return _LoadSecuritySheetPDFCommand ?? (_LoadSecuritySheetPDFCommand = new Command(async() => await ExecuteLoadSecuritySheetPDF())); }
        }

        private async Task ExecuteLoadSecuritySheetPDF ()
        {
            string pdfFilename = await WebApiPDFData.GetSecuritySheetURL();

            if (!string.IsNullOrWhiteSpace(pdfFilename))
            {
                var PDFViewPage = new PDFWebViewPage(Statics.WebApiURLs.webApiBaseUrl + "informes/pdfjs/web/viewer.html?file=" + Statics.WebApiURLs.webApiBaseUrl + pdfFilename)
                {
                    Title = TextResources.PDFView_SecuritySheetTitle,
                    Icon = new FileImageSource { File = pdfFilename }
                };

                await Navigation.PushAsync(PDFViewPage);
            }
            else
            {
                await DeviceMessages.ShowAlert(TextResources.Error, TextResources.PDFGet_SecuritySheet, "Ok");
            }
        }

        /*private Command _LoadLOPDPDFCommand;
        public Command LoadLOPDPDFCommand
        {
            get { return _LoadLOPDPDFCommand ?? (_LoadLOPDPDFCommand = new Command(async () => await ExecuteLoadLOPDPDF())); }
        }

        private async Task ExecuteLoadLOPDPDF()
        {
            string pdfFilename = await WebApiPDFData.GetLOPDURL();

            if (!string.IsNullOrWhiteSpace(pdfFilename))
            {
                var PDFViewPage = new PDFWebViewPage(Statics.WebApiURLs.webApiBaseUrl + "informes/pdfjs/web/viewer.html?file=" + Statics.WebApiURLs.webApiBaseUrl + pdfFilename)
                {
                    Title = TextResources.PDFView_LOPDTitle,
                    Icon = new FileImageSource { File = pdfFilename }
                };

                await Navigation.PushAsync(PDFViewPage);
            }
            else
            {
                await DeviceMessages.ShowAlert(TextResources.Error, TextResources.PDFGet_LOPD, "Ok");
            }
        }*/

        private Command _LoadLOPDPageCommand;
        public Command LoadLOPDPageCommand
        {
            get { return _LoadLOPDPageCommand ?? (_LoadLOPDPageCommand = new Command(async () => await ExecuteLoadLOPDPage())); }
        }

        private async Task ExecuteLoadLOPDPage()
        {
            await Navigation.PushModalAsync(new ShowTextPage()
            {
                Title = TextResources.LOPD_Title,
                BindingContext = new ShowTextViewModel(WebApiURLs.webApiTextLOPDUrl) { Navigation = this.Navigation }
            });
        }
        #endregion *********************************************************

        #region Send to SAP Commands ***************************************
        private Command _SendSignatureToSAP;
        public Command SendSignatureToSAP
        {
            get { return _SendSignatureToSAP ?? (_SendSignatureToSAP = new Command(async (signature) =>
                    {
                        if (signature is Stream)
                            await ExecuteSendSignatureToSAP((Stream)signature);
                    }));
            }
        }

        private async Task ExecuteSendSignatureToSAP(Stream signature)
        {
            DocumentSignatureSendModel documentSignatureSendModel;
            bool success = true;
            int sended = 0;
            bool sendByMail;
            bool toList;
            byte[] bSignature;
            string _error = TextResources.SigningError_init + Environment.NewLine;
            bool tempSuccess;


            if (IsBusy) return;

            if (signature != null)
            {
                IsBusy = true;
                SendSignatureToSAP.ChangeCanExecute();

                using (MemoryStream ms = new MemoryStream())
                {
                    signature.CopyTo(ms);
                    bSignature = ms.ToArray();
                }

                //Asking for mailing this document(s) (or not)
                switch (docType)
                {
                    case DocumentTypeModel.DocumentTypeEnum.Albaran:
                        sendByMail = await DeviceMessages.ConfirmationWindow(TextResources.SendByMail_title, TextResources.SendByMail_message, TextResources.SendByMail_yes, TextResources.SendByMail_no);
                        break;
                    case DocumentTypeModel.DocumentTypeEnum.Visitas:
                        sendByMail = false;
                        break;
                    case DocumentTypeModel.DocumentTypeEnum.Epis:
                        sendByMail = false;
                        break;
                    default:
                        sendByMail = await DeviceMessages.ConfirmationWindow(TextResources.SendByMail_title, TextResources.SendByMail_message, TextResources.SendByMail_yes, TextResources.SendByMail_no);
                        break;
                }

                sendingSigns = true;
                totalDocSending = documentList.Count;

                for (int i = 0; i < totalDocSending; i++)
                {
                    nDocSending = i + 1;
                    sendedPrcnt = (double)i / (double)totalDocSending;
                    nameDocSending = documentList[i].docNum;

                    documentSignatureSendModel = new DocumentSignatureSendModel
                    {
                        docType = documentList[i].docType,
                        docEntry = documentList[i].Id,
                        userCode = WebApiUserLoginData.loggedAccount.userName, //Services.WebApiUserLoginData.loggedAccount.idEmpleadoVentas,
                        signature = bSignature,

                        sendByMail = sendByMail //Recuperamos si se quiere mandar o no el correo
                    };

                    
                    switch (documentList[i].docType)
                    {
                        case (int)DocumentTypeModel.DocumentTypeEnum.Albaran:
                            documentSignatureSendModel.firmaNombre = ((DocumentSignerDataModel)documentList[i]).firmaNombre;
                            documentSignatureSendModel.firmaApellidos = ((DocumentSignerDataModel)documentList[i]).firmaApellidos;
                            documentSignatureSendModel.firmaNIF = ((DocumentSignerDataModel)documentList[i]).firmaNIF;
                            documentSignatureSendModel.firmaMatricula = ((DocumentSignerDataModel)documentList[i]).firmaMatricula;
                            break;
                        case (int)DocumentTypeModel.DocumentTypeEnum.Visitas:
                            break;
                        case (int)DocumentTypeModel.DocumentTypeEnum.Epis:
                            break;
                        default:
                            break;
                    }

                    tempSuccess = await WebApiSendSignatureData.SendSignature(documentSignatureSendModel);

                    if (tempSuccess)
                        sended++;
                    else
                    {
                        success = false;
                        if (multiDocumentMode)
                            _error += Environment.NewLine + "- " + documentList[i].docNum;
                    }
                }

                sendingSigns = false;

                //if everithing is Ok, asking to the user where to come back
                if (success)
                {
                    if (!multiDocumentMode)
                        toList = await DeviceMessages.ConfirmationWindow(TextResources.SigningSuccess_title, TextResources.SigningSuccess_message,
                                                                         TextResources.SigningSuccess_backList, TextResources.SigningSuccess_backMenu);
                    else
                        toList = await DeviceMessages.ConfirmationWindow(TextResources.SigningSuccess_title, TextResources.SigningSuccess_messageMulti,
                                                                         TextResources.SigningSuccess_backList, TextResources.SigningSuccess_backMenu);

                    if (toList)
                    {
                        //Remove all pages until there are only menu, document list and current page
                        while (Navigation.NavigationStack.Count > 3)
                            Navigation.RemovePage(Navigation.NavigationStack[2]);
                        //Then remove current page to show Document List
                        await Navigation.PopAsync();
                    }
                    else
                    {
                        await Navigation.PopToRootAsync();
                    }
                }
                else
                {
                    //En modo multidocumento, si se envía aunque sólo sea un documento, hay que repetir el proceso, porque la lista a enviar ha cambiado
                    //Si no se envía ninguno, se puede repetir el proceso porque puede ser problema de la red
                    if (multiDocumentMode && (sended > 0))
                    {
                        toList = await DeviceMessages.ConfirmationWindow(TextResources.General_Warning, _error,
                                                                         TextResources.SigningSuccess_backList, TextResources.SigningSuccess_backMenu);

                        if (toList)
                        {
                            //Remove all pages until there are only menu, document list and current page
                            while (Navigation.NavigationStack.Count > 3)
                                Navigation.RemovePage(Navigation.NavigationStack[2]);
                            //Then remove current page to show Document List
                            await Navigation.PopAsync();
                        }
                        else
                        {
                            await Navigation.PopToRootAsync();
                        }
                    }
                }

                SendSignatureToSAP.ChangeCanExecute();
                IsBusy = false;
            }
        }
        #endregion *********************************************************



        private void DataConversion(List<DocumentDataModel> _iniList)
        {
            //documentList = new List<DocumentSignerDataModel>();
            //costumerList = new List<DSCostumerData>();
            DocumentSignerDataViewModel.DSCostumerData tempCostumer;

            if (_iniList != null)
            {
                //Inicialización de las listas
                if (costumerList == null)
                    costumerList = new ObservableCollection<DocumentSignerDataViewModel.DSCostumerData>();
                if (costumerList.Count > 0)
                    costumerList.Clear();

                //Recorremos la lista de entrada convirtiendo los datos en la lista que se maneja en esta página y la siguiente(DocumentSignerDataModel)
                //y resumiendo los datos de los clientes
                for (int i = 0; i < _iniList.Count; i++)
                {
                    tempCostumer = costumerList.FirstOrDefault(x => x.Id == _iniList[i].cardCode);
                    if (tempCostumer == null)
                        costumerList.Add(new DocumentSignerDataViewModel.DSCostumerData()
                        {
                            Id = _iniList[i].cardCode,
                            Name = _iniList[i].cardName,
                            docList = _iniList[i].docNum
                        });
                    else
                        tempCostumer.docList = tempCostumer.docList + ", " + _iniList[i].docNum;
                }
            }

        }
    }
}
