﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReyvFirmaAPP.Services
{
    public interface INotifications
    {
        void SendNotification(string titulo, string mensaje);
        void ShowSmallText(string mensaje);
    }
}
