﻿using ReyvFirmaAPP.Localization;
using ReyvFirmaAPP.Models;
using ReyvFirmaAPP.Statics;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ReyvFirmaAPP.Services
{
    public static class WebApiUserLoginData
    {
        #region Login data *********************************************************************************
        public class LoggedAccount
        {
            public string userName;
            public string password;
            public string token;
            //public bool soyJefeVentas;
            public int idEmpleadoVentas;
            public string nombreEmpleadoVentas;
        }

        public static LoggedAccount loggedAccount { get; set; } = new LoggedAccount();

        public static bool isAuthenticated;
        #endregion *****************************************************************************************

        #region Data get and set ***************************************************************************
        static public void StoreUserLoginData(UserLoginModel datosRecibidos)
        {
            loggedAccount.userName = datosRecibidos.userName;
            loggedAccount.password = datosRecibidos.password;
            loggedAccount.token = datosRecibidos.token;
            loggedAccount.idEmpleadoVentas = datosRecibidos.idEmpleadoVentas;
            loggedAccount.nombreEmpleadoVentas = datosRecibidos.firstLastName();
        }


        static public string GetToken()
        {
            //Xamarin.Auth.Account miUsuario = Xamarin.Auth.AccountStore.Create().FindAccountsForService(WebApiUrls.webApiBaseUrl).First();
            return WebApiUserLoginData.loggedAccount.token;
        }

        #endregion *****************************************************************************************

        #region Data send **********************************************************************************
        async static public Task<HttpResponseMessage> EnvioSolicitudLogin(string _user, string _pwrd)
        {
            ObservableCollection<UserLoginModel> usrLoginItem;

            usrLoginItem = new ObservableCollection<UserLoginModel>();

            HttpClient client = new HttpClient
            {
                BaseAddress = new Uri(WebApiURLs.webApiBaseUrl)
            };


            //Setting header as JSON
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            //Creando parámetros JSON (Usuario y contraseña)
            byte[] bytearray = System.Text.Encoding.UTF8.GetBytes(_user + ":" + _pwrd);
            //Añadiendo autenticación tipo "Basic" a la cabecera con los parámetros
            client.DefaultRequestHeaders.Authorization =
                new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(bytearray));

            //Envío del mensaje
            HttpResponseMessage response;
            try
            {
                response = await client.PostAsync(WebApiURLs.webApiLoginUrl, null);
                //No hace falta un cuerpo, porque la info de login va en la autenticación, pero se añadirían campos JSON al body así
                //new StringContent(JsonConvert.SerializeObject(userLoginModel), System.Text.Encoding.UTF8, "application/json"));
            }
            catch (Exception ex)
            {
                response = new HttpResponseMessage(System.Net.HttpStatusCode.ServiceUnavailable);
                await DeviceMessages.ShowAlert(TextResources.Error, ex.Message, "OK");
            }

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
                isAuthenticated = true;
            else
                isAuthenticated = false;

            return response;
        }
        #endregion ****************************************************************************************
    }
}
