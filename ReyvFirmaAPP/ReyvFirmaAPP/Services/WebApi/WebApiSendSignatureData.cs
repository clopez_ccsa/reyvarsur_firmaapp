﻿using Newtonsoft.Json;
using ReyvFirmaAPP.Localization;
using ReyvFirmaAPP.Models;
using ReyvFirmaAPP.Statics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ReyvFirmaAPP.Services.WebApi
{
    public static class WebApiSendSignatureData
    {
        public static async Task<bool> SendSignature (DocumentSignatureSendModel document)
        {
            HttpResponseMessage response = null;
            HttpClient client = new HttpClient { BaseAddress = new Uri(WebApiURLs.webApiBaseUrl) };
            HttpContent content;
            bool _created = false;

            if (document != null)
            {
                try
                {
                    //Setting header as JSON
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    //Añadiendo Token a la cabecera
                    client.DefaultRequestHeaders.Add("Token", Services.WebApiUserLoginData.loggedAccount.token);
                    //Añadiendo datos al contenido
                    content = new StringContent(JsonConvert.SerializeObject(document), System.Text.Encoding.UTF8, "application/json");
                    
                    //Envío del mensaje
                    response = await WebApiBase.WebApiPostAsync(client, WebApiURLs.webApiSignatureSendUrl, content);

                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        string tempMessage = await response.Content.ReadAsStringAsync();
                        //Change the message if the returned code is positive
                        if (string.IsNullOrWhiteSpace(tempMessage))
                        {
                            _created = true;
                        }
                        else
                        {
                            await DeviceMessages.ShowAlert(TextResources.SAP_Answer, tempMessage, "OK");
                        }
                    }
                    else
                    {
                        await DeviceMessages.ShowAlert(TextResources.Error_Connection + " " + ((int)response.StatusCode).ToString() + ":", response.ReasonPhrase, "OK");
                    }
                }
                catch (Exception ex)
                {
                    await DeviceMessages.ShowAlert(TextResources.Error_ProgramException, ex.Message, "OK");
                }
            }

            return _created;
        }
    }
}
