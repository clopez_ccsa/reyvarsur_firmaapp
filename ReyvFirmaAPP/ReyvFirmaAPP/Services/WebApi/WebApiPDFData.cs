﻿using Newtonsoft.Json;
using ReyvFirmaAPP.Localization;
using ReyvFirmaAPP.Statics;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ReyvFirmaAPP.Services.WebApi
{
    public static class WebApiPDFData
    {
        public static async Task<string> GetPDFURL(string _url)
        {
            HttpResponseMessage response = null;
            HttpClient client = new HttpClient
            {
                BaseAddress = new Uri(WebApiURLs.webApiBaseUrl)
            };
            string tempString;
            string documentURL = null;

            try
            {
                //Setting header as JSON
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                //Añadiendo Token a la cabecera
                client.DefaultRequestHeaders.Add("Token", Services.WebApiUserLoginData.loggedAccount.token);

                //Envío del mensaje
                response = await WebApiBase.WebApiGetAsync(client, _url);
                //No hace falta un cuerpo, porque la info de login va en la autenticación, pero se añadirían campos JSON al body así
                //new StringContent(JsonConvert.SerializeObject(userLoginModel), System.Text.Encoding.UTF8, "application/json"));

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    tempString = await response.Content.ReadAsStringAsync();
                    documentURL = JsonConvert.DeserializeObject<string>(tempString);
                }
                else
                {
                    await DeviceMessages.ShowAlert(TextResources.Error_Connection + " " + ((int)response.StatusCode).ToString() + ":", response.ReasonPhrase, "OK");
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("****** EXCEPTION: " + ex);
                await DeviceMessages.ShowAlert("Excepción del programa:", ex.Message, "OK");
            }


            return documentURL;
        }


        public static async Task<string> GetSecuritySheetURL()
        {
            return await GetPDFURL(WebApiURLs.webApiSecuritySheetUrl);
        }

        public static async Task<string> GetLOPDURL()
        {
            return await GetPDFURL(WebApiURLs.webApiLOPDUrl);
        }
    }
}
