﻿using Newtonsoft.Json;
using ReyvFirmaAPP.Localization;
using ReyvFirmaAPP.Models;
using ReyvFirmaAPP.Statics;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ReyvFirmaAPP.Services.WebApi
{
    public static class WebApiDocumentsData
    {
        public static async Task<object> GetDocumentsList (string userCode, int docType)
        {
            string urlParams;
            HttpResponseMessage response = null;
            HttpClient client = new HttpClient
            {
                BaseAddress = new Uri(WebApiURLs.webApiBaseUrl)
            };
            string tempString;
            object documentList = null;

            try
            {
                //Setting header as JSON
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                //Añadiendo Token a la cabecera
                client.DefaultRequestHeaders.Add("Token", Services.WebApiUserLoginData.loggedAccount.token);

                urlParams = string.Format("?userCode={0}&docType={1}", userCode, docType);
                
                //Envío del mensaje
                response = await WebApiBase.WebApiGetAsync(client, WebApiURLs.webApiDocumentListUrl + urlParams);
                //No hace falta un cuerpo, porque la info de login va en la autenticación, pero se añadirían campos JSON al body así
                //new StringContent(JsonConvert.SerializeObject(userLoginModel), System.Text.Encoding.UTF8, "application/json"));

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    tempString = await response.Content.ReadAsStringAsync();
                    switch (docType)
                    {
                        case (int)DocumentTypeModel.DocumentTypeEnum.Albaran:
                            documentList = JsonConvert.DeserializeObject<List<DeliveryDataModel>>(tempString);
                            break;
                        case (int)DocumentTypeModel.DocumentTypeEnum.Traslados:
                            documentList = JsonConvert.DeserializeObject<List<InventoryTransferDataModel>>(tempString);
                            break;
                        case (int)DocumentTypeModel.DocumentTypeEnum.Visitas:
                            documentList = JsonConvert.DeserializeObject<List<VisitorDataModel>>(tempString);
                            break;
                        case (int)DocumentTypeModel.DocumentTypeEnum.Epis:
                            documentList = JsonConvert.DeserializeObject<List<EpisDataModel>>(tempString);
                            break;
                        default:
                            documentList = JsonConvert.DeserializeObject<List<DocumentDataModel>>(tempString);
                            break;
                    }
                }
                else
                {
                    await DeviceMessages.ShowAlert(TextResources.Error_Connection + " " + ((int)response.StatusCode).ToString() + ":", response.ReasonPhrase, "OK");
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("****** EXCEPTION: " + ex);
                await DeviceMessages.ShowAlert("Excepción del programa:", ex.Message, "OK");
            }


            return documentList;
        }


        public static async Task<List<DocumentLineDataModel>> GetDocumentLines(string docEntry, int docType)
        {
            string urlParams;
            HttpResponseMessage response = null;
            HttpClient client = new HttpClient
            {
                BaseAddress = new Uri(WebApiURLs.webApiBaseUrl)
            };
            string tempString;
            List<DocumentLineDataModel> documentLines = null;

            try
            {
                //Setting header as JSON
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                //Añadiendo Token a la cabecera
                client.DefaultRequestHeaders.Add("Token", Services.WebApiUserLoginData.loggedAccount.token);

                urlParams = string.Format("?docEntry={0}&docType={1}", docEntry, docType);

                //Envío del mensaje
                response = await WebApiBase.WebApiGetAsync(client, WebApiURLs.webApiDocumentLinesUrl + urlParams);
                //No hace falta un cuerpo, porque la info de login va en la autenticación, pero se añadirían campos JSON al body así
                //new StringContent(JsonConvert.SerializeObject(userLoginModel), System.Text.Encoding.UTF8, "application/json"));

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    tempString = await response.Content.ReadAsStringAsync();
                    switch (docType)
                    {
                        default:
                            documentLines = JsonConvert.DeserializeObject<List<DocumentLineDataModel>>(tempString);
                            break;
                    }
                }
                else
                {
                    await DeviceMessages.ShowAlert(TextResources.Error_Connection + " " + ((int)response.StatusCode).ToString() + ":", response.ReasonPhrase, "OK");
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("****** EXCEPTION: " + ex);
                await DeviceMessages.ShowAlert("Excepción del programa:", ex.Message, "OK");
            }


            return documentLines;
        }



        public static async Task<List<DocumentSignerDataModel>> GetDocumentSignInfo(int docType, string nombre, string apellido)
        {
            string urlParams;
            HttpResponseMessage response = null;
            HttpClient client = new HttpClient
            {
                BaseAddress = new Uri(WebApiURLs.webApiBaseUrl)
            };
            string tempString;
            List<DocumentSignerDataModel> docSignList = null;

            try
            {
                //Setting header as JSON
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                //Añadiendo Token a la cabecera
                client.DefaultRequestHeaders.Add("Token", Services.WebApiUserLoginData.loggedAccount.token);

                urlParams = string.Format("?docType={0}&nombre={1}&apellido={2}", docType, nombre, apellido);

                //Envío del mensaje
                response = await WebApiBase.WebApiGetAsync(client, WebApiURLs.webApiSignatureInfoGetUrl + urlParams);
                //No hace falta un cuerpo, porque la info de login va en la autenticación, pero se añadirían campos JSON al body así
                //new StringContent(JsonConvert.SerializeObject(userLoginModel), System.Text.Encoding.UTF8, "application/json"));

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    tempString = await response.Content.ReadAsStringAsync();
                    docSignList = JsonConvert.DeserializeObject<List<DocumentSignerDataModel>>(tempString);
                }
                else
                {
                    await DeviceMessages.ShowAlert(TextResources.Error_Connection + " " + ((int)response.StatusCode).ToString() + ":", response.ReasonPhrase, "OK");
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("****** EXCEPTION: " + ex);
                await DeviceMessages.ShowAlert("Excepción del programa:", ex.Message, "OK");
            }


            return docSignList;
        }
    }
}
