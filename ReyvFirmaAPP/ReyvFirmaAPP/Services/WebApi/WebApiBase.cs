﻿using ReyvFirmaAPP.Models;
using ReyvFirmaAPP.Statics;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ReyvFirmaAPP.Services
{
    public static class WebApiBase
    {
        public static readonly int maxReconnections = 5;
        public enum FunctType { Get, Put, Delete, Post }


        public static async Task<HttpResponseMessage> WebApiFunctAsync(HttpClient client, FunctType sendingFunct, string requestUri, HttpContent content)
        {
            HttpResponseMessage response = null;
            bool returnResponse = true;
            int reconnectionTries = 0;

            do
            {
                switch (sendingFunct)
                {
                    case FunctType.Get:
                        response = await client.GetAsync(requestUri);
                        break;
                    case FunctType.Put:
                        response = await client.PutAsync(requestUri, content);
                        break;
                    case FunctType.Delete:
                        response = await client.DeleteAsync(requestUri);
                        break;
                    case FunctType.Post:
                        response = await client.PostAsync(requestUri, content);
                        break;
                    default:
                        response = new HttpResponseMessage(System.Net.HttpStatusCode.BadRequest);
                        break;
                }
                

                //In case of disconnection, we try to reconnect. It's out of switch because we need to check other error StatusCodes from
                //the connection itself
                if ((response.StatusCode == System.Net.HttpStatusCode.Unauthorized) && (reconnectionTries < maxReconnections))
                {
                    DependencyService.Get<INotifications>().ShowSmallText(string.Format("Reconectando al servidor ({0}/{1}).", reconnectionTries + 1, maxReconnections));
                    response = await WebApiUserLoginData.EnvioSolicitudLogin(WebApiUserLoginData.loggedAccount.userName,
                                                                             WebApiUserLoginData.loggedAccount.password);
                    reconnectionTries++;
                    returnResponse = false;
                }
                else
                {
                    returnResponse = true;
                }

            } while (returnResponse == false);

            //response.StatusCode error handler
            if (!response.IsSuccessStatusCode)
            {
                System.Diagnostics.Debug.WriteLine("Error de conexión " + ((int)response.StatusCode).ToString() + ":", response.ReasonPhrase, "OK");
                await DeviceMessages.ShowAlert("Error de conexión " + ((int)response.StatusCode).ToString() + ":", response.ReasonPhrase, "OK");
            }


            return response;
        }


        #region You should use this functions ********************************************************************************************************
        public static async Task<HttpResponseMessage> WebApiGetAsync(HttpClient client, string requestUri)
        {
            return await WebApiFunctAsync(client, FunctType.Get, requestUri, null);
        }


        public static async Task<HttpResponseMessage> WebApiPutAsync(HttpClient client, string requestUri, HttpContent content)
        {
            return await WebApiFunctAsync(client, FunctType.Put, requestUri, content);
        }


        public static async Task<HttpResponseMessage> WebApiDeleteAsync(HttpClient client, string requestUri)
        {
            return await WebApiFunctAsync(client, FunctType.Delete, requestUri, null);
        }

        public static async Task<HttpResponseMessage> WebApiPostAsync(HttpClient client, string requestUri, HttpContent content)
        {
            return await WebApiFunctAsync(client, FunctType.Post, requestUri, content);
        }
        #endregion **********************************************************************************************************************************


        #region GET LIST OF BASEMODEL DATA **********************************************************************************************************
        async public static Task<List<BaseModel>> WebApiGetBaseModelList(string gbmlUrl)
        {
            HttpResponseMessage response = null;
            HttpClient client = new HttpClient
            {
                BaseAddress = new Uri(WebApiURLs.webApiBaseUrl)
            };
            string tempString;
            List<BaseModel> marcasList = null;

            try
            {
                //Setting header as JSON
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                //Añadiendo Token a la cabecera
                client.DefaultRequestHeaders.Add("Token", Services.WebApiUserLoginData.loggedAccount.token);

                //Envío del mensaje
                response = await WebApiBase.WebApiGetAsync(client, gbmlUrl);
                //No hace falta un cuerpo, porque la info de login va en la autenticación, pero se añadirían campos JSON al body así
                //new StringContent(JsonConvert.SerializeObject(userLoginModel), System.Text.Encoding.UTF8, "application/json"));

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    tempString = await response.Content.ReadAsStringAsync();
                    if (!string.IsNullOrWhiteSpace(tempString))
                        marcasList = JsonConvert.DeserializeObject<List<BaseModel>>(tempString);
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("****** GET: Código: " + ((int)response.StatusCode).ToString());
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("****** EXCEPTION: " + ex);
                await DeviceMessages.ShowAlert("Excepción del programa:", ex.Message, "OK");
            }


            return marcasList;
        }
        #endregion *******************************************************************************************************************************************
    }
}
