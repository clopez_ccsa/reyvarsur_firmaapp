﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReyvFirmaAPP.Services
{
    /// <summary>
    /// Allow to know connection information of the device
    /// </summary>
    public interface IDeviceConnection
    {
        /// <summary>
        /// Check if the device is connected to a network
        /// </summary>
        /// <returns></returns>
        bool IsConnected();
    }
}
