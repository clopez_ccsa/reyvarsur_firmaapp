﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReyvFirmaAPP.Services
{
    public static class DeviceMessages
    {
        public static async Task ShowAlert (string title, string message, string confirm)
        {
            await App.CurrentApp.MainPage.DisplayAlert(title, message, confirm);
        }

        public static async Task<bool> ConfirmationWindow (string title, string message, string confirm, string cancel)
        {
            return await App.CurrentApp.MainPage.DisplayAlert(title, message, confirm, cancel);
        }
    }
}
