﻿using ReyvFirmaAPP.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace ReyvFirmaAPP.Converters
{
    public class BooleanToImageBNColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is DocumentTypeModel)
            {
                DocumentTypeModel _value = (DocumentTypeModel)value;
                if (_value.Deleted)
                    return _value.IconInactive;
                else
                    return _value.IconActive;
            }
            else
                return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
