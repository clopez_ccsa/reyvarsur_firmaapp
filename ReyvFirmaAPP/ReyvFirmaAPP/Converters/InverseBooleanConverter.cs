﻿
using System;
using Xamarin.Forms;
using System.Globalization;

namespace ReyvFirmaAPP.Converters
{
    public class InverseBooleanConverter : IValueConverter
    {
        #region IValueConverter implementation
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
                return !(bool)value;
            else
            {
                return true;
            }
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
                return !(bool)value;
            else
                return true;
        }
        #endregion
        
    }
}

