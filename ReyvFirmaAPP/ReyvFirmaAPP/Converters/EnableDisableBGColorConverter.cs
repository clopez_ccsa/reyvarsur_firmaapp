﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace ReyvFirmaAPP.Converters
{
    public class EnableDisableBGColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((value is bool) && (parameter is Color))
            {
                if ((bool)value)
                    return parameter;
                else
                    return Statics.Palette._DisabledButtonBG;
            }
            else
                return parameter;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
