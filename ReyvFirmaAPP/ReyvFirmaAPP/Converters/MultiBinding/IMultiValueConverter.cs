﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReyvFirmaAPP.Converters.MultiBinding
{
    public interface IMultiValueConverter
    {
        object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture);
    }
}
