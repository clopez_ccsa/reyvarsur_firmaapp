﻿using ReyvFirmaAPP.Converters.MultiBinding;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace ReyvFirmaAPP.Converters
{
    public class AutocompletionFocusAndNotSelectedConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            bool autocompleteSelected = true;
            bool isFocused = false;

            if (values[0] is bool)
                autocompleteSelected = (bool)values[0];
            if (values[1] is bool)
                isFocused = (bool)values[1];

            return !autocompleteSelected && isFocused;
        }
    }
}
