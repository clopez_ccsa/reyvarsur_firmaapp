﻿using ReyvFirmaAPP.Statics;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace ReyvFirmaAPP.Converters
{
    public class SelectedColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
            {
                bool _value = (bool)value;
                if (!_value)
                    return Palette._MainColorLight;
                else
                    return Palette._SelectedColor;
            }
            else
                return parameter;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
