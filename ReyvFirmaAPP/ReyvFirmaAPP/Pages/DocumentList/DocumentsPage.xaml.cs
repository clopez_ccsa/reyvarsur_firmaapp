﻿using ReyvFirmaAPP.Localization;
using ReyvFirmaAPP.Models;
using ReyvFirmaAPP.ViewModels.DocumentList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ReyvFirmaAPP.Pages.DocumentList
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DocumentsPage : ContentPage
	{
		public DocumentsPage ()
		{
			InitializeComponent ();
		}

        #region FORM EVENTS ***********************************************************************************
        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (BindingContext != null)
                ((DocumentsViewModel)BindingContext).LoadDocumentListCommand.Execute(null);
        }


        private void DocumentItemTapped(object sender, ItemTappedEventArgs e)
        {
            DocumentsViewModel documentsViewModel = (DocumentsViewModel)BindingContext;

            if (documentsViewModel != null)
            {
                try
                {
                    DocumentDataModel _document = (DocumentDataModel)e.Item;

                    if (!documentsViewModel.multiSelection)
                        documentsViewModel.PushDocumentDetailsPageCommand.Execute(_document);
                    else
                        documentsViewModel.SelectUnselectDocumentCommand.Execute(_document);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("EXCEPCION CARGANDO DOCUMENTO: " + ex);
                }
            }
        }


        private void NextButtonTapped(object sender, EventArgs e)
        {
            if (BindingContext != null)
                ((DocumentsViewModel)BindingContext).PushDocumentListPageCommand.Execute(null);
        }
        #endregion ********************************************************************************************


    }
}