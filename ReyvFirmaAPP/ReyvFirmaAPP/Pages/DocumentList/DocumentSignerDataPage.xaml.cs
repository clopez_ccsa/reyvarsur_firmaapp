﻿using ReyvFirmaAPP.ViewModels.DocumentList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ReyvFirmaAPP.Pages.DocumentList
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DocumentSignerDataPage : ContentPage
    {
        public DocumentSignerDataPage()
        {
            InitializeComponent();
        }


        #region Buttons events ******************************************************************************************
        private void SignerNameEntry_OnTextChange(object sender, EventArgs e)
        {
            //Adding a delay between text changed and the search
            string value = ((TextChangedEventArgs)e).NewTextValue;

            Task.Run(async () =>
            {
                await Task.Delay(1000);
                
                if (((DocumentSignerDataViewModel)BindingContext).signerData.firmaNombre == value) //only search if the text is the same, user stopped typing
                {
                    ((DocumentSignerDataViewModel)BindingContext).UpdateDocSigningInfoCommand.Execute(null);
                }
            });
        }


        private void SignerLastNameEntry_OnTextChange(object sender, EventArgs e)
        {
            //Adding a delay between text changed and the search
            string value = ((TextChangedEventArgs)e).NewTextValue;

            Task.Run(async () =>
            {
                await Task.Delay(1000);
                
                if (((DocumentSignerDataViewModel)BindingContext).signerData.firmaApellidos == value) //only search if the text is the same, user stopped typing
                {
                    ((DocumentSignerDataViewModel)BindingContext).UpdateDocSigningInfoCommand.Execute(null);
                }
            });
        }
        #endregion ******************************************************************************************************

        #region Buttons events ******************************************************************************************
        private void NextButtonTapped(object sender, EventArgs e)
        {
            if (BindingContext != null)
                ((DocumentSignerDataViewModel)BindingContext).PushDocumentDetailsPageCommand.Execute(null);
        }

        private void BackButtonTapped(object sender, EventArgs e)
        {
            if (BindingContext != null)
                ((DocumentSignerDataViewModel)BindingContext).GoBackCommand.Execute(null);
        }

        private void AutocompleteItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (BindingContext != null)
                ((DocumentSignerDataViewModel)BindingContext).AutocompleteInfoCommand.Execute(e.Item);
        }

        #endregion ******************************************************************************************************

        /*private void ChangedBindingData(object sender, EventArgs e)
        {
            Models.DocumentDataModel data = (Models.DocumentDataModel)dataContentView.BindingContext;
            if (data != null)
            {
                switch (data.docType)
                {
                    case (int)Models.DocumentTypeModel.DocumentTypeEnum.Albaran:
                        dataContentView.Content = new Views.DocumentList.CustomerDocSummaryView() { Margin = new Thickness(3), BindingContext = dataContentView.BindingContext };
                        break;
                    case (int)Models.DocumentTypeModel.DocumentTypeEnum.Visitas:
                        dataContentView.Content = new Views.DocumentList.VisitorDetailsView() { Margin = new Thickness(3), BindingContext = dataContentView.BindingContext };
                        break;
                    case (int)Models.DocumentTypeModel.DocumentTypeEnum.Epis:
                        dataContentView.Content = new Views.DocumentList.EpisView() { Margin = new Thickness(3), BindingContext = dataContentView.BindingContext };
                        break;
                    default:
                        dataContentView.Content = new Views.DocumentList.DocumentDetailsView() { Margin = new Thickness(3), BindingContext = dataContentView.BindingContext };
                        break;
                }
            }
        }*/
    }
}