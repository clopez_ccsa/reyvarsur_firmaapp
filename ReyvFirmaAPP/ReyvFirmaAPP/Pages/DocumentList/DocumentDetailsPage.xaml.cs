﻿using ReyvFirmaAPP.ViewModels.DocumentList;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ReyvFirmaAPP.Pages.DocumentList
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DocumentDetailsPage : ContentPage
	{
        //private ListView mdListView;

        public DocumentDetailsPage ()
		{
			InitializeComponent ();
            InitializeMultipleDocumentData();
        }

        private void InitializeMultipleDocumentData ()
        {
            /*StackLayout mdStackLayout = new StackLayout() { Margin = new Thickness(3) };
            ScrollView mdScrollView = new ScrollView() { VerticalOptions = LayoutOptions.FillAndExpand };
            mdListView = new ListView()
            {
                HasUnevenRows = true,
                SeparatorColor = Statics.Palette._MainColor,
                SeparatorVisibility = SeparatorVisibility.Default,
                HorizontalOptions = LayoutOptions.FillAndExpand
            };

            mdListView.ItemTemplate = new DataTemplate(() =>
            {
                return new ViewCell() { View = new Views.DocumentList.CustomerDocSummaryView() };
            });

            Label mdSignerLabel = new Label()
            {
                TextColor = Statics.Palette._TextColorLight,
                FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                Margin = new Thickness(10, 0, 10, 5)
            };
            //mdSignerLabel.BindingContextChanged += 

            mdScrollView.Content = mdListView;
            mdStackLayout.Children.Add(mdScrollView);
            multiDataContentView.Content = mdStackLayout;*/
        }

        #region Buttons events ******************************************************************************************
        private async void NextButtonTapped(object sender, EventArgs e)
        {
            Stream bitmap;
            if (!signaturePadControl.IsBlank)
            {
                bitmap = await signaturePadControl.GetImageStreamAsync(SignaturePad.Forms.SignatureImageFormat.Jpeg, Color.Black, Color.White);
                if (BindingContext != null)
                    ((DocumentDetailsViewModel)BindingContext).SendSignatureToSAP.Execute(bitmap);
            }
        }

        private void SecuritySheetTapped(object sender, EventArgs e)
        {
            ((DocumentDetailsViewModel)BindingContext).LoadSecuritySheetPDFCommand.Execute(null);
        }

        private void LOPDTapped(object sender, EventArgs e)
        {
            ((DocumentDetailsViewModel)BindingContext).LoadLOPDPageCommand.Execute(null);
        }
        #endregion ******************************************************************************************************


        #region Signature events ****************************************************************************************
        private void SignatureStrokeCompleted(object sender, EventArgs e)
        {
            SignatureIsBlankUpdate();
        }

        private void SignatureCleared(object sender, EventArgs e)
        {
            SignatureIsBlankUpdate();
        }

        private void SignatureIsBlankUpdate()
        {
            if (BindingContext != null)
                ((DocumentDetailsViewModel)BindingContext).signIsBlank = signaturePadControl.IsBlank;
        }
        #endregion ******************************************************************************************************


        private void ChangedBindingData (object sender, EventArgs e)
        {
            DocumentDetailsViewModel tempBinding = (DocumentDetailsViewModel)dataContentView.BindingContext;
            if ((tempBinding != null) && (tempBinding.documentList != null) && (tempBinding.documentList.Count > 0))
            {
                Models.DocumentDataModel data = tempBinding.documentList[0];
                if (data != null)
                {
                    switch (data.docType)
                    {
                        case (int)Models.DocumentTypeModel.DocumentTypeEnum.Albaran:
                            dataContentView.Content = new Views.DocumentList.DeliveryDetailsView() { Margin = new Thickness(3), BindingContext = data };
                            break;
                        case (int)Models.DocumentTypeModel.DocumentTypeEnum.Traslados:
                            dataContentView.Content = new Views.DocumentList.InventoryTransferDetailsView() { Margin = new Thickness(3), BindingContext = data };
                            break;
                        case (int)Models.DocumentTypeModel.DocumentTypeEnum.Visitas:
                            dataContentView.Content = new Views.DocumentList.VisitorDetailsView() { Margin = new Thickness(3), BindingContext = data };
                            break;
                        case (int)Models.DocumentTypeModel.DocumentTypeEnum.Epis:
                            dataContentView.Content = new Views.DocumentList.EpisView() { Margin = new Thickness(3), BindingContext = data };
                            break;
                        default:
                            dataContentView.Content = new Views.DocumentList.DocumentDetailsView() { Margin = new Thickness(3), BindingContext = data };
                            break;
                    }
                }
            }
        }

        
    }
}