﻿using ReyvFirmaAPP.ViewModels.DocumentList;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ReyvFirmaAPP.Pages.DocumentList
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DocumentLinesPage : ContentPage
	{
		public DocumentLinesPage ()
		{
			InitializeComponent ();
		}

        #region Buttons events ******************************************************************************************
        private void NextButtonTapped(object sender, EventArgs e)
        {
            if (BindingContext != null)
                ((DocumentLinesViewModel)BindingContext).PushDocumentDetailsPageCommand.Execute(null);
        }

        private void BackButtonTapped(object sender, EventArgs e)
        {
            if (BindingContext != null)
                ((DocumentLinesViewModel)BindingContext).GoBackCommand.Execute(null);
        }

        #endregion ******************************************************************************************************

        private void ChangedBindingData(object sender, EventArgs e)
        {
            Models.DocumentDataModel data = (Models.DocumentDataModel)dataContentView.BindingContext;
            switch (data.docType)
            {
                case (int)Models.DocumentTypeModel.DocumentTypeEnum.Visitas:
                    dataContentView.Content = new Views.DocumentList.VisitorDetailsView() { Margin = new Thickness(3), BindingContext = dataContentView.BindingContext };
                    break;
                case (int)Models.DocumentTypeModel.DocumentTypeEnum.Epis:
                    dataContentView.Content = new Views.DocumentList.EpisView() { Margin = new Thickness(3), BindingContext = dataContentView.BindingContext };
                    break;
                default:
                    dataContentView.Content = new Views.DocumentList.DocumentDetailsView() { Margin = new Thickness(3), BindingContext = dataContentView.BindingContext };
                    break;
            }
        }
    }
}