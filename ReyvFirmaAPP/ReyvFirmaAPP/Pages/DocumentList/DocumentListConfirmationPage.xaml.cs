﻿using ReyvFirmaAPP.ViewModels.DocumentList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ReyvFirmaAPP.Pages.DocumentList
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DocumentListConfirmationPage : ContentPage
    {
        public DocumentListConfirmationPage()
        {
            InitializeComponent();
        }

        private void BackButtonTapped(object sender, EventArgs e)
        {
            if (BindingContext != null)
                ((DocumentListConfirmationViewModel)BindingContext).GoBackCommand.Execute(null);
        }
        private void NextButtonTapped(object sender, EventArgs e)
        {
            if (BindingContext != null)
                ((DocumentListConfirmationViewModel)BindingContext).PushDocumentDetailsPageCommand.Execute(null);
        }
    }
}