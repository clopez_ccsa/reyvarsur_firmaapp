﻿using Newtonsoft.Json;
using ReyvFirmaAPP.Models;
using ReyvFirmaAPP.Pages.Base;
using ReyvFirmaAPP.Services;
using ReyvFirmaAPP.Statics;
using ReyvFirmaAPP.ViewModels.Splash;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ReyvFirmaAPP.Pages.Splash
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SplashPage : SplashPageXaml
    {
        private bool enVertical = true;

        public SplashPage()
        {
            InitializeComponent();

            BindingContext = new SplashViewModel();
            //ViewModel.Mensaje = Localization.TextResources.Splash_SigningIn;
            ViewModel.Mensaje = "";

            SignInButton.GestureRecognizers.Add(
                new TapGestureRecognizer()
                {
                    NumberOfTapsRequired = 1,
                    Command = new Command(SignInButtonTapped)
                });
        }



        #region //Acciones a llevar a cabo si se rota la pantalla ***********************************************
        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);
            //Si la pantalla se pone en horizontal
            if ((width > height) && enVertical)
            {
                //grid_Main.RowDefinitions.RemoveAt(grid_Main.RowDefinitions.Count - 1);
                //grid_Main.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

                //Modificamos el grid con las filas y columnas que necesitemos.
                //Crearlo de cero es mejor que andar creando y destruyendo filas y columnas, porque no podemos estar seguros de
                //cuántas veces se va a ejecutar el evento
                grid_Main.RowDefinitions = new RowDefinitionCollection
                {
                    new RowDefinition()
                };
                grid_Main.ColumnDefinitions = new ColumnDefinitionCollection
                {
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) }
                };

                //Modificamos la posición sólo del grid donde se introducen los datos, la imágen siempre va a estar en la celda (0,0)
                grid_Input.SetValue(Grid.ColumnProperty, 1);
                grid_Input.SetValue(Grid.RowProperty, 0);
                //Otra froma de hacerlo
                //Grid.SetColumn(grid_Input, 1);
                //Grid.SetRow(grid_Input, 0);

                //Seteamos la variable enVertical a false
                enVertical = false;
            }
            //Si la pantalla se pone en vertical
            else if ((width < height) && !enVertical)
            {
                //Modificamos el grid con las filas y columnas que necesitemos.
                grid_Main.RowDefinitions = new RowDefinitionCollection
                {
                    new RowDefinition { Height = new GridLength(1, GridUnitType.Star) },
                    new RowDefinition { Height = new GridLength(1, GridUnitType.Star) },
                    new RowDefinition { Height = new GridLength(RowSizes.MediumRowHeightDouble, GridUnitType.Absolute) }
                };
                grid_Main.ColumnDefinitions = new ColumnDefinitionCollection
                {
                    new ColumnDefinition()
                };

                //Modificamos la posición sólo del grid donde se introducen los datos, la imágen siempre va a estar en la celda (0,0)
                grid_Input.SetValue(Grid.ColumnProperty, 0);
                grid_Input.SetValue(Grid.RowProperty, 1);
                //Seteamos la variable enVertical a false
                enVertical = true;
            }
        }
        #endregion


        #region Acciones a llevar a cabo al pulsar el botón ***********************************************************
        private async void SignInButtonTapped()
        {
            try
            {
                await App.ExecuteIfConnected(async () =>
                {
                    ViewModel.Mensaje = Localization.TextResources.Splash_SigningIn;

                    if (await Authenticate())
                    {
                        // Pop off the modally presented SplashPage.
                        // Note that we're not popping the ADAL auth UI here; that's done automatcially by the ADAL library when the Authenticate() method returns.
                        App.GoToMainPage();

                        // Broadcast a message that we have sucessdully authenticated.
                        // This is mostly just for Android. We need to trigger Android to call the SalesDashboardPage.OnAppearing() method,
                        // because unlike iOS, Android does not call the OnAppearing() method each time that the Page actually appears on screen.
                        MessagingCenter.Send(this, MessagingServiceConstants.AUTHENTICATED);
                    }
                });
            }
            catch (Exception ex)
            {
                await DeviceMessages.ShowAlert(ex.Message, ex.StackTrace, "OK");
            }
        }

        private async Task<bool> Authenticate()
        {
            bool success = false;

            try
            {
                HttpResponseMessage response = await WebApiUserLoginData.EnvioSolicitudLogin(UsernameEntry.Text, PasswordEntry.Text);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    //Recuperamos los datos de la respuesta
                    UserLoginModel datosRecibidos = JsonConvert.DeserializeObject<UserLoginModel>(await response.Content.ReadAsStringAsync());
                    //Usamos esos datos para crear una cuenta de usuario local
                    WebApiUserLoginData.StoreUserLoginData(datosRecibidos);

                    success = true;
                }
                else
                {
                    await DisplayAlert("Error de conexión " + ((int)response.StatusCode).ToString() + ":", response.ReasonPhrase, "OK");
                }

            }
            catch (Exception ex)
            {
                await DeviceMessages.ShowAlert(ex.Message, ex.StackTrace, "OK");
            }
            finally
            {
                ViewModel.Mensaje = "";
            }

            return success;
        }
        #endregion ****************************************************************************************************
    }

    /// <summary>
    /// This class definition just gives us a way to reference ModelBoundContentPage<T> in the XAML of this Page.
    /// </summary>
    public abstract class SplashPageXaml : ModelBoundContentPage<SplashViewModel> { }
}