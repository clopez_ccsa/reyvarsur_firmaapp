﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ReyvFirmaAPP.Pages.PDFPages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PDFWebViewPage : ContentPage
    {
        /// <summary>
        /// Crea una nueva página, con una WebView, que carga un PDF que se le pasa por parámetro
        /// </summary>
        /// <param name="_uri">Servidor + ruta fichero viewer.html?file= + Servidor + rutaPDF</param>
        // Ruta de ejemplo: "http://192.168.1.127:59214/" + "informes/pdfjs/web/viewer.html?file=" +
        //                  "http://192.168.1.127:59214/" + "informes/1v3.pdf"
        public PDFWebViewPage(string pdfFilename)
        {
            string _uri;
            InitializeComponent();

            _uri = pdfFilename;
            nameWebView.Uri = _uri; //Esto debe estar después de la inicialización

        }

    }
}