﻿using ReyvFirmaAPP.ViewModels.ShowDBData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ReyvFirmaAPP.Pages.ShowDBData
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ShowTextPage : ContentPage
	{
		public ShowTextPage ()
		{
			InitializeComponent ();
		}

        private void BackButtonTapped(object sender, EventArgs e)
        {
            ((ShowTextViewModel)BindingContext).GoBackCommand.Execute(null);
        }
    }
}