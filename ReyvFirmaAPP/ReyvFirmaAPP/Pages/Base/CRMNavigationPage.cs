﻿using ReyvFirmaAPP.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ReyvFirmaAPP.Pages.Base
{
    public class CRMNavigationPage : NavigationPage
    {
        public CRMNavigationPage(Page root)
            : base(root)
        {
            Init();
        }

        public CRMNavigationPage()
        {
            Init();
        }

        void Init()
        {
            BarBackgroundColor = Palette._MainColor;
            BarTextColor = Palette._ContrastMainColor;
            //Popped += HideNavBar;
            //Pushed += ShowNavBar;
        }

        /*private void ShowNavBar(object sender, NavigationEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(IsVisible);
            NavigationPage.SetHasNavigationBar(this, true);
        }

        private void HideNavBar(object sender, NavigationEventArgs e)
        {
            if (Navigation.NavigationStack.Count <= 1)
                NavigationPage.SetHasNavigationBar(this, false);
        }*/
    }
}
