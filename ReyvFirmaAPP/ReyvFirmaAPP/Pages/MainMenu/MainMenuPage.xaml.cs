﻿using ReyvFirmaAPP.Models;
using ReyvFirmaAPP.ViewModels.MainMenu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ReyvFirmaAPP.Pages.MainMenu
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MainMenuPage : ContentPage
	{
		public MainMenuPage ()
		{
			InitializeComponent ();

            BindingContext = new MainMenuViewModel() { Navigation = this.Navigation };
        }

        protected override void OnAppearing()
        {
            NavigationPage.SetHasNavigationBar(this, false);

            base.OnAppearing();
        }

        private void Item_OnClick(object sender, ItemTappedEventArgs e)
        {
            if ((e.Item is DocumentTypeModel) && (BindingContext != null))
            {
                if (!((DocumentTypeModel)e.Item).Deleted)
                {
                    ((MainMenuViewModel)BindingContext).LoadDocumentCommand.Execute(e.Item);
                    NavigationPage.SetHasNavigationBar(this, true);
                }
            }
        }
    }
}