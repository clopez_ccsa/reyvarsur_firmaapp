﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ReyvFirmaAPP.Statics
{
    public static class FontSize
    {
        public readonly static double XLarge = Device.GetNamedSize(NamedSize.Large, typeof(Label)) * 2;
        public readonly static double XXLarge = Device.GetNamedSize(NamedSize.Large, typeof(Label)) * 4;
        public readonly static double XXXLarge = Device.GetNamedSize(NamedSize.Large, typeof(Label)) * 6;
        public readonly static double Huge = Device.GetNamedSize(NamedSize.Large, typeof(Label)) * 8;
        public readonly static double _120PercentOfSmall = Device.GetNamedSize(NamedSize.Small, typeof(Label)) * 1.2;
        public readonly static double _150PercentOfLarge = Device.GetNamedSize(NamedSize.Large, typeof(Label)) * 1.5;
    }
}
