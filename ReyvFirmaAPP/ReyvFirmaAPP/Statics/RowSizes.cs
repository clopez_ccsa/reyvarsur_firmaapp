﻿

namespace ReyvFirmaAPP.Statics
{
    public static class RowSizes
    {
        public readonly static double XXLargeRowHeightDouble = 100;
        public readonly static double XLargeRowHeightDouble = 80;
        public readonly static double LargeRowHeightDouble = 60;
        public readonly static double MediumRowHeightDouble = 44;
        public readonly static double SmallMediumRowHeightDouble = 37;
        public readonly static double SmallRowHeightDouble = 30;
        public readonly static double XSmallRowHeightDouble = 20;

        public static int XXLargeRowHeightInt { get { return (int)XXLargeRowHeightDouble; } }
        public static int XLargeRowHeightInt { get { return (int)XLargeRowHeightDouble; } }
        public static int LargeRowHeightInt { get { return (int)LargeRowHeightDouble; } }
        public static int MediumRowHeightInt { get { return (int)MediumRowHeightDouble; } }
        public static int SmallRowHeightInt { get { return (int)SmallRowHeightDouble; } }
    }
}

