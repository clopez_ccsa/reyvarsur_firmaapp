﻿using Xamarin.Forms;

namespace ReyvFirmaAPP.Statics
{
    public static class Palette
    {
        public static readonly Color _MainColor         = Color.FromHex("005DB4");
        public static readonly Color _MainColorLight    = Color.FromHex("FFFFFF");
        public static readonly Color _MainColorDark     = Color.FromHex("C7004B");
        public static readonly Color _MainColorGrayed   = Color.FromHex("E5CDA0");
        //public static readonly Color _TextColor         = Color.FromRgb(102, 112, 88);
        public static readonly Color _TextColor         = Color.FromRgb(64, 64, 64);
        public static readonly Color _TextColorLight    = Color.FromRgb(128, 128, 128);
        public static readonly Color _SelectedColor     = Color.FromRgb(186, 203, 219);
        //public static readonly Color _MainColor         = Color.FromHex("6BAE2B");
        //public static readonly Color _MainColorLight    = Color.FromHex("B7E28B");
        //public static readonly Color _MainColorDark     = Color.FromHex("29772C");

        public static readonly Color _HighlightNegativeBG   = Color.FromRgb(228, 213, 175);
        public static readonly Color _HighlightPositiveBG   = Color.FromRgb(247, 249, 151);
        public static readonly Color _DisabledButtonBG      = Color.FromRgb(144, 146, 148);

        //Contrast colors (if color is dark, this is light but wit some influence of main color)
        public static readonly Color _ContrastMainColor     = Color.FromRgb(231, 242, 251);
        public static readonly Color _ContrastMainColorDark = Color.FromRgb(255, 244, 249);
    }
}
