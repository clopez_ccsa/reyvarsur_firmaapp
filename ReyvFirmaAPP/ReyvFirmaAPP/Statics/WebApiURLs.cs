﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReyvFirmaAPP.Statics
{
    public static class WebApiURLs
    {
        //Cliente BBDD REYVARSUR
        //public readonly static string webApiBaseUrl = "http://81.45.140.249:59214/";  //cliente externa productivo
        //public readonly static string webApiBaseUrl = "http://81.45.140.249:59215/";  //cliente externa pruebas
        //public readonly static string webApiBaseUrl = "http://192.168.1.9:59214/";  //cliente interna productiva <--- Usar esta dirección!!!!
        //public readonly static string webApiBaseUrl = "http://192.168.1.9:59215/";  //cliente interna pruebas <--- Usar esta dirección!!!!

        //Cliente BBDD REDUYMECA
        //public readonly static string webApiBaseUrl = "http://81.45.140.249:59216/";  //cliente externa productivo
        //public readonly static string webApiBaseUrl = "http://81.45.140.249:59217/";  //cliente externa pruebas
        //public readonly static string webApiBaseUrl = "http://192.168.1.9:59216/";  //cliente interna productiva
        //public readonly static string webApiBaseUrl = "http://192.168.1.9:59217/";  //cliente interna pruebas
        //public readonly static string webApiBaseUrl = "http://192.168.1.11:59216/";  //cliente interna productiva HANA10 <--- Usar esta dirección!!!!
        public readonly static string webApiBaseUrl = "http://192.168.1.11:59217/";  //cliente interna pruebas HANA10 <--- Usar esta dirección!!!!

        //CCSA testeo
        //public readonly static string webApiBaseUrl = "http://85.136.102.58:49995/";  //Máquina App externa con puerto CALO
        //public readonly static string webApiBaseUrl = "http://85.136.102.58:49996/";  //Máquina App externa con puerto AMM
        //public readonly static string webApiBaseUrl = "http://85.136.102.58:49997/";  //Máquina App externa
        //public readonly static string webApiBaseUrl = "http://192.168.1.127:59214/";  //Máquina App
        //public readonly static string webApiBaseUrl = "http://192.168.1.127:49998/";  //Máquina App IIS
        //public readonly static string webApiBaseUrl = "http://192.168.1.192:59214/";  //Máquina App SAP10


        public enum ClienteEnum { Reyvarsur, Reduymeca, CCSA };
        //public readonly static ClienteEnum clienteEnum = ClienteEnum.CCSA;
        public readonly static ClienteEnum clienteEnum = ClienteEnum.Reduymeca;

        public readonly static string webApiLoginUrl = "api/login/login";
        public readonly static string webApiLoginCheckUrl = "api/login/check";

        public readonly static string webApiDocumentListUrl = "api/documents/list";
        public readonly static string webApiDocumentLinesUrl = "api/documents/lines";
        public readonly static string webApiSignatureInfoGetUrl = "api/documents/getSignInfo";
        public readonly static string webApiSignatureSendUrl = "api/documents/sign";

        public readonly static string webApiSecuritySheetUrl = "api/documents/securitySheet";
        public readonly static string webApiLOPDUrl = "api/documents/lopd";

        public readonly static string webApiTextLOPDUrl = "api/text/lopd";
    }
}
