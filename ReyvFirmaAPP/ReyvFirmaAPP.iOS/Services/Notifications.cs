﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using Xamarin.Forms;
using ReyvFirmaAPP.iOS.Services;
using CoreGraphics;
using UserNotifications;

[assembly: Dependency(typeof(Notifications))]
namespace ReyvFirmaAPP.iOS.Services
{
    public class Notifications : ReyvFirmaAPP.Services.INotifications
    {
        private const int viewToastTag = 1989;

        #region private methods *********************************************************************************************
    
        private void myToast (string mensaje)
        {
            UIWindow keyWindow;
            UIView rootView;
            UIView residualView;

            UIView viewToast;
            UILabel lblMsg;

            nfloat screenWidth;
            nfloat screenHeight;
            CGSize textSize;

            //UIAlertController alertController = UIAlertController.Create(titulo, mensaje, UIAlertControllerStyle.Alert);
            //alertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));

            screenWidth = UIScreen.MainScreen.Bounds.Width; //* UIScreen.MainScreen.Scale;
            screenHeight = UIScreen.MainScreen.Bounds.Height; //* UIScreen.MainScreen.Scale;

            //Check if there is a previous notification
            keyWindow = UIApplication.SharedApplication.KeyWindow;
            rootView = keyWindow.RootViewController.View;
            residualView = rootView.ViewWithTag(viewToastTag);
            if (residualView != null)
                residualView.RemoveFromSuperview();

            //Making the label with the text
            lblMsg = new UILabel();
            lblMsg.Lines = System.Text.RegularExpressions.Regex.Matches(mensaje, Environment.NewLine).Count + 1;
            lblMsg.Text = mensaje;
            lblMsg.TextColor = UIColor.White;
            lblMsg.TextAlignment = UITextAlignment.Center;
            //Settings the size of the text
            textSize = lblMsg.Text.StringSize(lblMsg.Font);
            textSize.Width = (nfloat)Math.Min(textSize.Width + 20, screenWidth);
            textSize.Height = textSize.Height * (lblMsg.Lines + 1);
            //Resizing label to the size of text
            lblMsg.Frame = new CGRect(0, 0, textSize.Width, textSize.Height);
            

            //Making the box/view that will contains the text label
            viewToast = new UIView(new CGRect((screenWidth / 2.0f) - (textSize.Width / 2f),
                                    screenHeight * 5.0f / 6.0f,
                                    textSize.Width, textSize.Height));
            viewToast.BackgroundColor = UIColor.FromRGBA(0, 121, 255, 200);
            viewToast.Tag = viewToastTag;

            //Setting center and dependencies
            //viewToast.Center = rootView.Center;
            viewToast.AddSubview(lblMsg);
            rootView.AddSubview(viewToast);
            //roundtheCorner(viewToast);

            //Animation block
            UIView.BeginAnimations("Toast");
            UIView.SetAnimationDuration(5.0f);
            UIView.SetAnimationTransition(UIViewAnimationTransition.CurlUp, viewToast, false);
            viewToast.Alpha = 0.0f;
            UIView.CommitAnimations();
        }

        private void SendEnhancedUserNotification (string titulo, string mensaje)
        {
            UNMutableNotificationContent content;
            UNTimeIntervalNotificationTrigger trigger;
            string requestID;
            UNNotificationRequest request;

            content = new UNMutableNotificationContent();
            content.Title = titulo;
            content.Body = mensaje;
            content.Badge = 1;

            trigger = UNTimeIntervalNotificationTrigger.CreateTrigger(1f, false);

            requestID = "CRMNotification";
            request = UNNotificationRequest.FromIdentifier(requestID, content, trigger);

            UNUserNotificationCenter.Current.AddNotificationRequest(request, (err) =>
           {
               if (err != null)
               {
                    //Do something with the error
                }
           });

        }

        private void SendDeprecatedUserNotification(string titulo, string mensaje)
        {
            UILocalNotification notification;

            if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            {
                var notificationSettings = UIUserNotificationSettings.GetSettingsForTypes(
                    UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound, null);
                UIApplication.SharedApplication.RegisterUserNotificationSettings(notificationSettings);
            }

            notification = new UILocalNotification();
            notification.FireDate = NSDate.FromTimeIntervalSinceNow(0f);

            notification.AlertTitle = titulo;
            notification.AlertBody = mensaje;
            notification.ApplicationIconBadgeNumber = 1;

            UIApplication.SharedApplication.ScheduleLocalNotification(notification);

            notification.ApplicationIconBadgeNumber = 0;
        }

        #endregion *************************************************************************************************

        #region public methods *************************************************************************************

        public void SendNotification(string titulo, string mensaje)
        {
            /*CoreFoundation.DispatchQueue.MainQueue.DispatchAsync(() =>
                    myToast(titulo + ":" + Environment.NewLine +  mensaje));*/
            if (UIDevice.CurrentDevice.CheckSystemVersion(10, 0))
                CoreFoundation.DispatchQueue.MainQueue.DispatchAsync(() => SendEnhancedUserNotification(titulo, mensaje));
            else
                CoreFoundation.DispatchQueue.MainQueue.DispatchAsync(() => SendDeprecatedUserNotification(titulo, mensaje));
        }

        public void ShowSmallText(string mensaje)
        {
            CoreFoundation.DispatchQueue.MainQueue.DispatchAsync(() => myToast(mensaje));
        }
        #endregion *************************************************************************************************
    }
}