﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using ReyvFirmaAPP.iOS.Renderers;
using ReyvFirmaAPP.Views.PDFViews;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(PDFWebView), typeof(PDFWebViewRendererIOS))]
namespace ReyvFirmaAPP.iOS.Renderers
{
    public class PDFWebViewRendererIOS : ViewRenderer<PDFWebView, UIWebView>
    {
        /// <summary>
        /// Esto debería funcionar en iOS. Su única misión es cargar la URL
        /// </summary>
        /// <param name="e"></param>
        protected override void OnElementChanged(ElementChangedEventArgs<PDFWebView> e)
        {
            base.OnElementChanged(e);

            if (Control == null)
                SetNativeControl(new UIWebView());

            if (e.OldElement != null)
            {

            }
            if (e.NewElement != null)
            {
                PDFWebView customWebView = Element as PDFWebView;
                Control.LoadRequest(new NSUrlRequest(new NSUrl(customWebView.Uri)));
                Control.ScalesPageToFit = true;
            }
        }
    }
}